package com.yoavst.mashov.controller

import android.util.Log
import com.yoavst.mashov.android.successNotNullUi
import com.yoavst.mashov.controller.ApiController.Api
import com.yoavst.mashov.controller.RefreshController.Callback
import com.yoavst.mashov.isForbidden
import com.yoavst.mashov.isNeedToLogin
import com.yoavst.mashov.model.ApiLevelException
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import uy.kohesive.injekt.injectLazy
import java.util.*

object RefreshControllerImpl : RefreshController {
    private val apiController by injectLazy<ApiController>()
    private val databaseController by injectLazy<DatabaseController>()
    private val callbacks: MutableMap<Int, Callback> = HashMap(2)

    private var isPerformingLogin = false
    private var shouldPerformLogin = false
    val runningRequests: MutableList<Api> = ArrayList(10)
    val queuedRequests: MutableList<Api> = ArrayList(10)

    override fun init() {
        apiController.attachRawDataProcessor { json, api ->
            when (api) {
                Api.Alfon -> databaseController.setContacts(json)
                Api.BagrutGrades -> databaseController.setBagrutGrades(json)
                Api.BehaveEvents -> databaseController.setBehaveEvents(json)
                Api.Grades -> databaseController.setGrades(json)
                Api.Groups -> databaseController.setGroups(json)
                Api.LessonsCount -> databaseController.setLessonsCount(json)
                Api.Messages -> databaseController.setMessages(json)
                Api.Timetable -> databaseController.setTimetable(json)
                Api.Message -> {
                    val id = """"messageId":"([A-Za-z0-9-]+)"""".toRegex().find(json)!!.groups.last()!!.value
                    databaseController.setMessage(id, json)
                }
                else -> return@attachRawDataProcessor true
            }
            false
        }
    }

    /**
     * given an api.
     * If the api is now querying the server - false
     * If the api is on queued requests - false
     * If performing login now - add to queued, false
     */
    override fun refresh(api: Api): Boolean {
        if (shouldPerformLogin) {
            queuedRequests += api
            loginAgain()
        }
        if (api in runningRequests) return false
        else if (api in queuedRequests) return false
        else if (isPerformingLogin) {
            queuedRequests += api
            return false
        } else {
            refreshInternal(api)
            return true
        }
    }

    private fun refreshInternal(api: Api) {
        runningRequests += api
        @Suppress("NON_EXHAUSTIVE_WHEN")
        when (api) {
            Api.Alfon -> apiController.getAlfon(databaseController.sessionId, databaseController.userId)
            Api.BagrutGrades -> apiController.getBagrutGrades(databaseController.sessionId, databaseController.userId)
            Api.BehaveEvents -> apiController.getBehaveEvents(databaseController.sessionId, databaseController.userId)
            Api.Grades -> apiController.getGrades(databaseController.sessionId, databaseController.userId)
            Api.Groups -> apiController.getGroups(databaseController.sessionId, databaseController.userId)
            Api.LessonsCount -> apiController.getLessonsCount(databaseController.sessionId, databaseController.userId)
            Api.Messages -> apiController.getMessages(databaseController.sessionId)
            Api.Timetable -> apiController.getTimetable(databaseController.sessionId, databaseController.userId)
            else -> null
        }?.successUi {
            runningRequests -= api
            callbacks.values.forEach { it.onSuccess(api) }
        }?.failUi {
            it.printStackTrace()
            if (it is ApiLevelException) {
                callbacks.values.forEach { it.onOldApi() }
            } else if (it.isNeedToLogin()) {
                loginAgain()
            } else if (it.isForbidden()) {
                runningRequests -= api
                callbacks.values.forEach { it.onSuspend() }
            } else {
                runningRequests -= api
                callbacks.values.forEach { it.onFail(api) }
            }
        }
    }

    override fun attachCallback(id: Int, callback: Callback) {
        callbacks += id to callback
    }

    override fun detachCallback(id: Int) {
        callbacks.keys -= id
    }

    private fun loginAgain() {
        if (!isPerformingLogin) {
            isPerformingLogin = true
            callbacks.values.forEach { it.onLogin() }
            queuedRequests += runningRequests
            runningRequests.clear()
            Log.e("Mashov", "Performing login")
            apiController.login(databaseController.school!!, databaseController.username, databaseController.password, databaseController.year) successNotNullUi {
                Log.e("Mashov", "login succeed")
                databaseController.bulk {
                    this.sessionId = it.data.sessionId
                    this.year = it.data.year
                }
                isPerformingLogin = false
                shouldPerformLogin = false
                queuedRequests.forEach { refreshInternal(it) }
                queuedRequests.clear()
            } failUi {
                Log.e("Mashov", "login failed")
                isPerformingLogin = false
                shouldPerformLogin = false
                queuedRequests.clear()
                runningRequests.clear()
                if (it.isNeedToLogin()) {
                    databaseController.clearData()
                    callbacks.values.forEach { it.onUnauthorized() }
                } else if (it.isForbidden()) {
                    callbacks.values.forEach { it.onSuspend() }
                } else {
                    shouldPerformLogin = true
                    callbacks.values.forEach { it.onLoginFail() }
                }
            }
        }

    }
}