package com.yoavst.mashov.controller

import com.yoavst.mashov.model.*

interface DatabaseController {
    //region User info
    var id: String
    var password: String
    var school: School?
    var username: String
    var sessionId: String
    var userId: String
    var year: Int
    var name: String
    var classCode: String
    var classNum: Int
    var databaseVersion: Int
    var csrfToken: String
    var uniqueId: String
    var mashovSessionId: String
    //endregion

    //region Getters
    val profilePicturePath: String
    val grades: List<Grade>?
    val bagrutGrades: List<BagrutGrade>?
    val behaveEvents: List<BehaveEvent>?
    val groups: List<Group>?
    val lessonsCount: List<LessonCount>?
    val timetable: List<Lesson>?
    val contacts: List<Contact>?
    val messages: List<MessageTitle>?
    fun getMessage(messageId: String): Message?
    fun hasMessage(messageId: String): Boolean
    //endregion

    //region Setters
    fun setGrades(json: String?)
    fun setBagrutGrades(json: String?)
    fun setBehaveEvents(json: String?)
    fun setGroups(json: String?)
    fun setLessonsCount(json: String?)
    fun setTimetable(json: String?)
    fun setContacts(json: String?)
    fun setMessages(json: String?)
    fun setMessage(messageId: String, json: String?)
    //endregion

    fun hasEnoughData(): Boolean
    fun clearData()
    fun bulk(operation: DatabaseController.() -> Unit)
}