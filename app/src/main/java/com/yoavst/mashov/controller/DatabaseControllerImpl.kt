package com.yoavst.mashov.controller

import com.chibatching.kotpref.KotprefModel
import com.chibatching.kotpref.bulk
import com.yoavst.mashov.android.App
import com.yoavst.mashov.model.*
import uy.kohesive.injekt.injectLazy
import java.io.File

object DatabaseControllerImpl : DatabaseController, KotprefModel() {
    private val dataParser: DataParser by injectLazy()

    override var id: String by stringPrefVar()
    private var _school: String by stringPrefVar()
    override var school: School?
        get() {
            return _school.let { if (it.isEmpty()) null else it.split(Divider).let { School(it[0].toInt(), it[1], it[2].split(',').toIntArray()) } }
        }
        set(value) {
            _school = if (value == null) "" else value.id.toString() + Divider + value.name + Divider + value.years.joinToString(",")
        }
    override var sessionId: String by stringPrefVar()
    override var userId: String by stringPrefVar()
    override var password: String by stringPrefVar()
    override var username: String by stringPrefVar()
    override var year: Int by intPrefVar()
    override var name: String by stringPrefVar()
    override var classCode: String by stringPrefVar()
    override var classNum: Int by intPrefVar()
    override var databaseVersion: Int by intPrefVar(5)
    override var csrfToken: String by stringPrefVar()
    override var uniqueId: String by stringPrefVar()
    override var mashovSessionId: String by stringPrefVar()

    override val grades: List<Grade>?
        get() = if (!GradesFile.exists()) null else dataParser.grades(GradesFile.readText())
    override val bagrutGrades: List<BagrutGrade>?
        get() = if (!BagrutGradesFile.exists()) null else dataParser.bagrutGrades(BagrutGradesFile.readText())
    override val behaveEvents: List<BehaveEvent>?
        get() = if (!BehaveEventsFile.exists()) null else dataParser.behaveEvents(BehaveEventsFile.readText())
    override val groups: List<Group>?
        get() = if (!GroupsFile.exists()) null else dataParser.groups(GroupsFile.readText())
    override val lessonsCount: List<LessonCount>?
        get() = if (!LessonsCountFile.exists()) null else dataParser.lessonsCount(LessonsCountFile.readText())
    override val timetable: List<Lesson>?
        get() = if (!TimetableFile.exists()) null else dataParser.timetable(TimetableFile.readText())
    override val contacts: List<Contact>?
        get() = if (!ContactsFile.exists()) null else dataParser.alfon(ContactsFile.readText())
    override val messages: List<MessageTitle>?
        get() = if (!MessagesFile.exists()) null else dataParser.messages(MessagesFile.readText())

    override fun setGrades(json: String?) {
        initStorage()
        if (json == null) GradesFile.delete()
        else GradesFile.writeText(json)
    }

    override fun setBagrutGrades(json: String?) {
        initStorage()
        if (json == null) BagrutGradesFile.delete()
        else BagrutGradesFile.writeText(json)
    }

    override fun setBehaveEvents(json: String?) {
        initStorage()
        if (json == null) BehaveEventsFile.delete()
        else BehaveEventsFile.writeText(json)
    }

    override fun setGroups(json: String?) {
        initStorage()
        if (json == null) GroupsFile.delete()
        else GroupsFile.writeText(json)
    }

    override fun setLessonsCount(json: String?) {
        initStorage()
        if (json == null) LessonsCountFile.delete()
        else LessonsCountFile.writeText(json)
    }

    override fun setTimetable(json: String?) {
        initStorage()
        if (json == null) TimetableFile.delete()
        else TimetableFile.writeText(json)
    }

    override fun setContacts(json: String?) {
        initStorage()
        if (json == null) ContactsFile.delete()
        else ContactsFile.writeText(json)
    }

    override fun setMessages(json: String?) {
        initStorage()
        if (json == null) MessagesFile.delete()
        else MessagesFile.writeText(json)
    }


    override fun getMessage(messageId: String): Message? {
        val file = messageFile(messageId)
        if (!file.exists()) return null
        return dataParser.message(file.readText())
    }

    override fun hasMessage(messageId: String) = messageFile(messageId).exists()

    override fun setMessage(messageId: String, json: String?) {
        if (json == null)
            messageFile(messageId).delete()
        else messageFile(messageId).writeText(json)
    }

    override fun bulk(operation: DatabaseController.() -> Unit) {
        this.bulk<DatabaseControllerImpl>(operation)
    }

    override fun hasEnoughData(): Boolean {
        return File(profilePicturePath).exists() && TimetableFile.exists() && GroupsFile.exists() && GradesFile.exists() && BehaveEventsFile.exists() && MessagesFile.exists()
    }

    override fun clearData() {
        if (!FilesFolder.deleteRecursively()) FilesFolder.deleteRecursively()
        clear()
    }

    internal val FilesFolder = File("/data/data/${App.packageName}/files/")
    internal val MessagesFolder = File(FilesFolder, "messages/")
    internal val GradesFile = File(FilesFolder, "grades.json")
    internal val BagrutGradesFile = File(FilesFolder, "bagrutGrades.json")
    internal val BehaveEventsFile = File(FilesFolder, "behaveEvents.json")
    internal val GroupsFile = File(FilesFolder, "groups.json")
    internal val LessonsCountFile = File(FilesFolder, "lessonsCount.json")
    internal val TimetableFile = File(FilesFolder, "timetable.json")
    internal val ContactsFile = File(FilesFolder, "contacts.json")
    internal val MessagesFile = File(FilesFolder, "messages.json")
    override val profilePicturePath: String = File(FilesFolder, "profile.jpg").absolutePath
    private val Divider = '%'
    private fun messageFile(messageId: String) = File(MessagesFolder, "$messageId.json")
    fun initStorage() {
        FilesFolder.mkdirs()
        MessagesFolder.mkdirs()
    }

    private fun List<String>.toIntArray(): IntArray {
        val array = IntArray(size)
        forEachIndexed { i, s -> array[i] = s.toInt() }
        return array
    }
}