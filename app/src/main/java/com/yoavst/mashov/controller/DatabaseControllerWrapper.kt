package com.yoavst.mashov.controller

import com.yoavst.mashov.model.*
import uy.kohesive.injekt.injectLazy
import java.util.*


/**
 * Created by yoavst.
 */
class DatabaseControllerWrapper(val databaseController: DatabaseController) : DatabaseController by databaseController {
    private val dataParser: DataParser by injectLazy()

    override var id: String
        get() {
            if (_id == null) {
                _id = databaseController.id
            }
            return _id!!
        }
        set(value) {
            _id = value
            storeInDB { databaseController.id = value }
        }
    override var name: String
        get() {
            if (_name == null) {
                _name = databaseController.name
            }
            return _name!!
        }
        set(value) {
            _name = value
            storeInDB { databaseController.name = value }
        }
    override var classCode: String
        get() {
            if (_classCode == null) {
                _classCode = databaseController.classCode
            }
            return _classCode!!
        }
        set(value) {
            _classCode = value
            storeInDB { databaseController.classCode = value }
        }
    override var classNum: Int
        get() {
            if (_classNum == null) {
                _classNum = databaseController.classNum
            }
            return _classNum!!
        }
        set(value) {
            _classNum = value
            storeInDB { databaseController.classNum = value }
        }
    override var sessionId: String
        get() {
            if (_sessionId == null) {
                _sessionId = databaseController.sessionId
            }
            return _sessionId!!
        }
        set(value) {
            _sessionId = value
            storeInDB { databaseController.sessionId = value }
        }
    override var userId: String
        get() {
            if (_userId == null) {
                _userId = databaseController.userId
            }
            return _userId!!
        }
        set(value) {
            _userId = value
            storeInDB { databaseController.userId = value }
        }
    override var username: String
        get() {
            if (_userName == null) {
                _userName = databaseController.username
            }
            return _userName!!
        }
        set(value) {
            _userName = value
            storeInDB { databaseController.username = value }
        }
    override var csrfToken: String
        get() {
            if (_csrfToken == null) {
                _csrfToken = databaseController.csrfToken
            }
            return _csrfToken!!
        }
        set(value) {
            _csrfToken = value
            storeInDB { databaseController.csrfToken = value }
        }

    override var uniqueId: String
        get() {
            if (_uniqueId == null) {
                _uniqueId = databaseController.uniqueId
            }
            return _uniqueId!!
        }
        set(value) {
            _uniqueId = value
            storeInDB { databaseController.uniqueId = value }
        }

    override var mashovSessionId: String
        get() {
            if (_mashovSessionId == null) {
                _mashovSessionId = databaseController.mashovSessionId
            }
            return _mashovSessionId!!
        }
        set(value) {
            _mashovSessionId = value
            storeInDB { databaseController.mashovSessionId = value }
        }

    private var _id: String? = null
    private var _sessionId: String? = null
    private var _userId: String? = null
    private var _userName: String? = null
    private var _name: String? = null
    private var _classCode: String? = null
    private var _classNum: Int? = null
    private var _csrfToken: String? = null
    private var _mashovSessionId: String? = null
    private var _uniqueId: String? = null


    override val grades: List<Grade>?
        get() {
            if (_grades == null) {
                _grades = databaseController.grades?.sortGrades()
            }
            return _grades
        }
    override val bagrutGrades: List<BagrutGrade>?
        get() {
            if (_bagrutGrades == null) {
                _bagrutGrades = databaseController.bagrutGrades
            }
            return _bagrutGrades
        }
    override val behaveEvents: List<BehaveEvent>?
        get() {
            if (_behaveEvents == null) {
                _behaveEvents = databaseController.behaveEvents?.sortEvents()
            }
            return _behaveEvents
        }
    override val groups: List<Group>?
        get() {
            if (_groups == null) {
                _groups = databaseController.groups
            }
            return _groups
        }
    override val lessonsCount: List<LessonCount>?
        get() {
            if (_lessonsCount == null) {
                _lessonsCount = databaseController.lessonsCount
            }
            return _lessonsCount
        }
    override val timetable: List<Lesson>?
        get() {
            if (_timetable == null) {
                _timetable = databaseController.timetable
            }
            return _timetable
        }
    override val contacts: List<Contact>?
        get() {
            if (_contacts == null) {
                _contacts = databaseController.contacts?.sortContacts()
            }
            return _contacts
        }
    override val messages: List<MessageTitle>?
        get() {
            if (_messages == null) {
                _messages = databaseController.messages?.sortMessages()
            }
            return _messages
        }


    var _grades: List<Grade>? = null
    var _bagrutGrades: List<BagrutGrade>? = null
    var _behaveEvents: List<BehaveEvent>? = null
    var _groups: List<Group>? = null
    var _lessonsCount: List<LessonCount>? = null
    var _timetable: List<Lesson>? = null
    var _contacts: List<Contact>? = null
    var _messages: List<MessageTitle>? = null

    override fun setGrades(json: String?) {
        databaseController.setGrades(json)
        _grades = if (json == null) null else dataParser.grades(json).sortGrades()
    }


    override fun setMessages(json: String?) {
        databaseController.setMessages(json)
        _messages = if (json == null) null else dataParser.messages(json).sortMessages()
    }

    override fun setContacts(json: String?) {
        databaseController.setContacts(json)
        _contacts = if (json == null) null else dataParser.alfon(json).sortContacts()
    }

    override fun setTimetable(json: String?) {
        databaseController.setTimetable(json)
        _timetable = if (json == null) null else dataParser.timetable(json)
    }

    override fun setGroups(json: String?) {
        databaseController.setGroups(json)
        _groups = if (json == null) null else dataParser.groups(json)
    }

    override fun setBehaveEvents(json: String?) {
        databaseController.setBehaveEvents(json)
        _behaveEvents = if (json == null) null else dataParser.behaveEvents(json).sortEvents()
    }

    override fun setBagrutGrades(json: String?) {
        databaseController.setBagrutGrades(json)
        _bagrutGrades = if (json == null) null else dataParser.bagrutGrades(json)
    }

    override fun setLessonsCount(json: String?) {
        databaseController.setLessonsCount(json)
        _lessonsCount = if (json == null) null else dataParser.lessonsCount(json)
    }

    private var bulkEnabled = false

    override fun bulk(operation: DatabaseController.() -> Unit) {
        bulkEnabled = true
        operation()
        bulkEnabled = false
        databaseController.bulk(operation)
    }

    private inline fun storeInDB(operation: () -> Unit) {
        if (!bulkEnabled) operation()
    }

    private fun List<Grade>.sortGrades() = sortedWith(Comparator<Grade> { l, r ->
        val date = r.eventDate - l.eventDate
        if (date != 0L) return@Comparator if (date < 0) -1 else 1
        else r.subject.compareTo(l.subject)
    })

    private fun List<MessageTitle>.sortMessages() = sortedWith(Comparator<MessageTitle> { l, r ->
        val date = r.sendDate - l.sendDate
        if (date != 0L) return@Comparator  if (date < 0) -1 else 1
        else r.subject.compareTo(l.subject)
    })

    private fun List<BehaveEvent>.sortEvents() = sortedWith(Comparator<BehaveEvent> { l, r ->
        val date = r.date - l.date
        if (date != 0L) return@Comparator if (date < 0) -1 else 1
        else r.subject.compareTo(l.subject)
    })

    private fun List<Contact>.sortContacts() = sortedWith(Comparator<Contact> { l, r ->
        val familyName = l.familyName.compareTo(r.familyName)
        if (familyName != 0) return@Comparator familyName
        else l.privateName.compareTo(r.privateName)
    })
}