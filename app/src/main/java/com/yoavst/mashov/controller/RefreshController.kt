package com.yoavst.mashov.controller

import com.yoavst.mashov.controller.ApiController.Api
interface RefreshController {
    fun refresh(api: Api): Boolean

    fun attachCallback(id: Int, callback: Callback)
    fun detachCallback(id: Int)

    fun init()

    interface Callback {
        fun onSuccess(api: Api)
        fun onFail(api: Api)
        fun onLoginFail()
        fun onSuspend()
        fun onUnauthorized()
        fun onLogin()
        fun onOldApi()
    }

}