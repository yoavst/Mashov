package com.yoavst.mashov.android.adapter

import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.yoavst.mashov.R
import com.yoavst.mashov.android.adapter.SchoolsAdapter.SchoolViewHolder
import com.yoavst.mashov.model.School
import kotlinx.android.synthetic.main.login_school_item.view.*
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.onClick

internal class SchoolsAdapter(var schools: List<School>, private val callback: (School) -> Unit) : Adapter<SchoolViewHolder>() {
    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.title.text = schools[position].name
        holder.itemView.onClick { callback(schools[position]) }
    }

    override fun getItemCount(): Int = schools.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SchoolViewHolder(parent.context.layoutInflater.inflate(R.layout.login_school_item, parent, false))

    class SchoolViewHolder(view: View) : ViewHolder(view) {
        val title: TextView = itemView.text1
    }
}