package com.yoavst.mashov.android.activity

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v4.widget.DrawerLayout.SimpleDrawerListener
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.yoavst.mashov.R
import com.yoavst.mashov.android.fragment.*
import com.yoavst.mashov.android.hide
import com.yoavst.mashov.android.isNetworkAvailable
import com.yoavst.mashov.android.show
import com.yoavst.mashov.controller.ApiController
import com.yoavst.mashov.controller.ApiController.Api
import com.yoavst.mashov.controller.DatabaseController
import com.yoavst.mashov.controller.RefreshController
import com.yoavst.mashov.controller.RefreshController.Callback
import com.yoavst.mashov.model.Refreshable
import kotlinx.android.synthetic.main.drawer_header.view.*
import kotlinx.android.synthetic.main.main_activity.*
import org.jetbrains.anko.itemsSequence
import org.jetbrains.anko.startActivity
import uy.kohesive.injekt.injectLazy
import java.util.*


class MainActivity : AppCompatActivity(), Callback {
    private val refreshController by injectLazy<RefreshController>()
    private val databaseController by injectLazy<DatabaseController>()
    val errorString by lazy { getString(R.string.request_failed) }
    private val successString by lazy { getString(R.string.refresh_success) }
    private val refreshThoseString by lazy { getString(R.string.refresh_those) }
    val noConnectionString by lazy { getString(R.string.no_connection_text) }
    private val apiValues by lazy { resources.getStringArray(R.array.apis) }
    private val hideMessageBar = Runnable { messageBar.hide(); currentlyShownApis.clear() }
    private val handler = Handler()
    private val fragmentStack: Stack<Int> = Stack()

    private var queuedId: Int = -1
    private var currentlyShowDialog: AlertDialog? = null
    private val currentlyShownApis: MutableList<String> = ArrayList(10)
    private lateinit var userAvatar: Bitmap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        initDrawer()
        openFragment(R.id.home)
        refreshController.attachCallback(1, this)
    }

    override fun onSuccess(api: Api) {
        currentlyShownApis += apiValues[api.ordinal]
        showMessage(successString.format(currentlyShownApis.joinToString(", ")), false)
        (supportFragmentManager.findFragmentById(R.id.fragment) as? Refreshable)?.let { fragment ->
            if (fragment.getApi() == null || fragment.getApi() == api) {
                fragment.onFinishRefresh()
            }
        }
    }

    override fun onFail(api: Api) {
        showMessage(errorString.format(apiValues[api.ordinal]))
        (fragmentManager.findFragmentById(R.id.fragment) as? Refreshable)?.let { fragment ->
            if (fragment.getApi() == null || fragment.getApi() == api) {
                fragment.onFailedToRefresh()
            }
        }
    }

    override fun onOldApi() {
        currentlyShowDialog = AlertDialog.Builder(this).setTitle(R.string.old_api_title).setMessage(R.string.old_api_text).setNeutralButton(R.string.accept, { _, _ ->
            finish()
        }).show()
    }

    override fun onLoginFail() {
        showMessage(getString(R.string.login_fail))
    }

    override fun onSuspend() {
        if (currentlyShowDialog == null) {
            currentlyShowDialog = AlertDialog.Builder(this)
                    .setMessage(R.string.http_forbidden_text)
                    .setPositiveButton(R.string.accept) { _, _ ->
                    }.show()
        }
    }

    override fun onUnauthorized() {
        currentlyShowDialog?.dismiss()
        currentlyShowDialog = AlertDialog.Builder(this)
                .setMessage(R.string.login_data_has_changed)
                .setPositiveButton(R.string.accept) { _, _ ->
                    startActivity<LoginActivity>()
                    finish()
                }.show()
    }

    override fun onLogin() {
        showMessage(getString(R.string.doing_login))
    }

    override fun onResume() {
        super.onResume()
        currentlyShowDialog?.show()
    }

    override fun onPause() {
        super.onPause()
        currentlyShowDialog?.hide()
    }

    override fun onDestroy() {
        super.onDestroy()
        refreshController.detachCallback(1)
    }

    fun showMessage(text: String, shouldClearApiList: Boolean = true) {
        if (shouldClearApiList)
            currentlyShownApis.clear()
        handler.removeCallbacks(hideMessageBar)
        messageBar.show()
        messageText.text = text
        handler.postDelayed(hideMessageBar, 3000)
    }

    private fun initDrawer() {
        if (Build.VERSION.SDK_INT >= 21) {
            // If there is drawer layout
            if (navigationView != null)
                window.statusBarColor = Color.TRANSPARENT
        }
        navigationView.setNavigationItemSelectedListener {
            queuedId = it.itemId
            drawerLayout.closeDrawer(navigationView)
            true
        }
        navigationView.inflateHeaderView(R.layout.drawer_header).apply {
            username.text = databaseController.name
            val options = BitmapFactory.Options()
            options.inPreferredConfig = Bitmap.Config.RGB_565
            userAvatar = BitmapFactory.decodeFile(databaseController.profilePicturePath, options)
            avatar.setImageBitmap(userAvatar)
        }
        drawerLayout.addDrawerListener(object : SimpleDrawerListener() {
            override fun onDrawerClosed(drawerView: View) {
                if (queuedId != -1) openFragment(queuedId)
            }
        })
    }

    fun openFragment(id: Int, backStack: Boolean = false) {
        if (!backStack)
            fragmentStack.clear()
        fragmentStack.add(id)

        val fragment = when (id) {
            R.id.home -> DashboardFragment()
            R.id.grades -> GradesFragment()
            R.id.averages -> AveragesFragment()
            R.id.bagrut -> BagrutGradesFragment()
            R.id.timetable -> TimetablePagerFragment()
            R.id.events -> BehaveEventsFragment()
            R.id.lessonsCount -> LessonsCountFragment()
            R.id.contacts -> ContactsFragment()
            R.id.inbox -> MessagesFragment()
            else -> throw IllegalArgumentException("No such id")
        }

        supportFragmentManager.beginTransaction().replace(R.id.fragment, fragment).commit()
        queuedId = -1
        navigationView.menu.itemsSequence().forEach { it.isChecked = false }
        navigationView.menu.findItem(id).isChecked = true
    }

    override fun onBackPressed() {
        if (fragmentStack.size > 1) {
            fragmentStack.pop() // current fragment
            openFragment(fragmentStack.peek())
        } else super.onBackPressed()
    }

    fun openDrawer() {
        drawerLayout.openDrawer(navigationView)
    }


    fun refresh(vararg apis: ApiController.Api): Boolean {
        if (isNetworkAvailable()) {
            apis.forEach { refreshController.refresh(it) }
            showMessage(refreshThoseString + apis.map { apiValues[it.ordinal] }.joinToString(", "))
            return true

        }
        showMessage(noConnectionString)
        return false
    }

    fun logout() {
        databaseController.clearData()
        startActivity<LoginActivity>()
        finish()
    }

}