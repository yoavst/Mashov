package com.yoavst.mashov.android

import android.app.Application
import com.chibatching.kotpref.Kotpref
import com.yoavst.mashov.controller.CookieManagerImpl
import com.yoavst.mashov.controller.DatabaseController
import com.yoavst.mashov.controller.DatabaseControllerImpl
import com.yoavst.mashov.injection.ControllerInjectionModule
import nl.komponents.kovenant.android.startKovenant
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get
import java.io.File

/**
 * The [Application] class of this application.
 * Currently it is only used as static Context and as a initializer
 * for injection and controllers.
 */
internal class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Instance = this
        Injekt.importModule(ControllerInjectionModule)
        Kotpref.init(this)
        DatabaseControllerImpl.initStorage()
        if (DatabaseControllerImpl.databaseVersion < 3) {
            File("/data/data/${App.packageName}/").deleteRecursively()
            DatabaseControllerImpl.databaseVersion = 3
        }
        if (DatabaseControllerImpl.databaseVersion < 4) {
            if (DatabaseControllerImpl.userId.isNotEmpty()) {
                DatabaseControllerImpl.username = DatabaseControllerImpl.userId
            }
            DatabaseControllerImpl.databaseVersion = 4
        }
        if (DatabaseControllerImpl.databaseVersion < 5) {
            DatabaseControllerImpl.setMessages(null)
            DatabaseControllerImpl.databaseVersion = 5
        }
        Injekt.get<DatabaseController>().let {
            CookieManagerImpl.csrfToken = it.csrfToken
            CookieManagerImpl.mashovSessionId = it.mashovSessionId
            CookieManagerImpl.uniquId = it.uniqueId
        }

        CookieManagerImpl.attachListener(1) {
            Injekt.get<DatabaseController>().let {
                it.csrfToken = CookieManagerImpl.csrfToken
                it.mashovSessionId = CookieManagerImpl.mashovSessionId
                it.uniqueId = CookieManagerImpl.uniquId
            }

        }
        startKovenant()
    }

    companion object {
        lateinit var Instance: App
        val packageName: String by lazy { Instance.packageName }
        val DEBUG_NO_LOADING_SCREEN = false
    }
}
