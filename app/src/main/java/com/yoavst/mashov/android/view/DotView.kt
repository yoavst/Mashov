package com.yoavst.mashov.android.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import com.yoavst.mashov.android.use

class DotView : View {
    private var color: Int = 0
        set(value) {
            field = value
            paint.color = value
        }
    private var paint: Paint = Paint()

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        context.obtainStyledAttributes(attrs, intArrayOf(android.R.attr.textColor)) use {
            color = it.getColor(0, Color.BLACK)
        }
    }

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context) : this(context, null)

    override fun onDraw(canvas: Canvas) {
        canvas.drawCircle((width / 2).toFloat(), (height / 2).toFloat(), (width / 3).toFloat(), paint)
    }
}