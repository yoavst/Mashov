package com.yoavst.mashov.android.activity

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.webkit.MimeTypeMap
import android.webkit.WebView
import android.webkit.WebViewClient
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import com.yoavst.mashov.R
import com.yoavst.mashov.StoragePermission
import com.yoavst.mashov.android.isNetworkAvailable
import com.yoavst.mashov.controller.DatabaseController
import kotlinx.android.synthetic.main.messages_activity.*
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast
import uy.kohesive.injekt.injectLazy
import java.io.File
import java.net.URL

internal class MessageActivity : AppCompatActivity() {
    private val id by lazy { intent.getStringExtra(Intent.EXTRA_UID) ?: "" }
    private val databaseController by injectLazy<DatabaseController>()
    private var currentDownload: Pair<String, Pair<String, String>>? = null
    private var currentlyShowDialog: AlertDialog? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.messages_activity)
        val message = databaseController.getMessage(id)!!
        toolbar.title = ""
        setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_material)
        toolbar.setNavigationOnClickListener { finish() }
        titleView.text = message.subject
        webView.settings.javaScriptEnabled = true
        val sb = StringBuilder()
        sb.append("<HTML><HEAD><LINK href=\"style.css\" type=\"text/css\" rel=\"stylesheet\"/></HEAD><body>")
                .append(message.body)
        sb.append("</body></HTML>")
        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                view?.scrollBy(0, 1000)
            }
        }
        webView.loadDataWithBaseURL("file:///android_asset/", sb.toString(), "text/html", "UTF-8", null)
        if (message.attachments.isNotEmpty()) {
            attachment.show()
            attachment.onClick {
                val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), message.attachments.first().name)
                if (file.exists())
                    launch(file)
                else if (isNetworkAvailable()) {
                    val id = message.messageId
                    val attachment = message.attachments.first()
                    if (ActivityCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") == 0)
                        download(id, attachment.id, attachment.name)
                    else {
                        currentDownload = id to (attachment.id to attachment.name)
                        ActivityCompat.requestPermissions(this, StoragePermission, PermissionRequest)

                    }
                } else
                    toast(R.string.no_connection_text)

            }
        }
    }

    fun download(id: String, fileId: String, name: String) {
        currentDownload = null
        var fileName = name
        val dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        if (File(dir, fileName).exists()) {
            var count = 1
            val (n, ext) = File(dir, fileName).let { it.nameWithoutExtension to it.extension }
            while (true) {
                if (!File(dir, n + count.toString() + "." + ext).exists())
                    break
                count++
            }
            fileName = n + count.toString() + "." + ext
        }
        Fuel.get("https://google.com")
                .bugFix(Uri.parse("https://svc.mashov.info/api/mail/messages/$id/files/$fileId/download").buildUpon().appendPath(name).build().toString())
                .header("Cookie" to "MashovSessionID=${databaseController.mashovSessionId}; Csrf-Token=${databaseController.csrfToken};uniquId=${databaseController.uniqueId}")
                .timeout(10000)
                .response { _, _, result ->
                    currentlyShowDialog?.hide()
                    currentlyShowDialog = null
                    result.success {
                        try {
                            val file = File(dir, fileName).apply { writeBytes(it) }
                            launch(file)
                        } catch (exception: ActivityNotFoundException) {
                            toast(R.string.downloaded_cannot_open)
                        }

                    }
                    result.failure {
                        toast(R.string.download_failed)
                    }
                }
        currentlyShowDialog = AlertDialog.Builder(this)
                .setMessage(R.string.loading_data)
                .setCancelable(false)
                .show()

    }

    private fun launch(file: File) = try {
        var type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(file.extension)
        if (type == null)
            type = "*/*"
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(FileProvider.getUriForFile(applicationContext, packageName + ".fileprovider", file), type)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        startActivity(intent)
    } catch (e: ActivityNotFoundException) {
        toast(R.string.cant_open_file)
    }

    private fun Request.bugFix(url: String): Request {
        this.path = url
        this.url = URL(url)
        return this
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PermissionRequest) {
            if (grantResults.isNotEmpty() && grantResults[0] == 0)
                download(currentDownload!!.first, currentDownload!!.second.first, currentDownload!!.second.second)
        }
    }

    override fun onResume() {
        super.onResume()
        currentlyShowDialog?.show()
    }

    override fun onPause() {
        super.onPause()
        currentlyShowDialog?.hide()
    }

    companion object {
        fun start(context: Context, id: String) {
            context.startActivity<MessageActivity>(Intent.EXTRA_UID to id)
        }

        private const val PermissionRequest = 42
    }
}