package com.yoavst.mashov.android.activity

import android.graphics.Bitmap.Config.RGB_565
import android.graphics.BitmapFactory
import android.graphics.BitmapFactory.Options
import android.graphics.Point
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import com.yoavst.mashov.R
import com.yoavst.mashov.android.*
import com.yoavst.mashov.controller.ApiController
import com.yoavst.mashov.controller.ApiController.Api
import com.yoavst.mashov.controller.DatabaseController
import com.yoavst.mashov.controller.RefreshController
import com.yoavst.mashov.controller.RefreshController.Callback
import com.yoavst.mashov.isForbidden
import com.yoavst.mashov.isNeedToLogin
import kotlinx.android.synthetic.main.loading_activity.*
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.dip
import org.jetbrains.anko.onClick
import org.jetbrains.anko.startActivity
import uy.kohesive.injekt.injectLazy
import java.io.File

internal class LoadingActivity : AppCompatActivity() {
    private val apiController by injectLazy<ApiController>()
    private val databaseController by injectLazy<DatabaseController>()
    private val refreshController by injectLazy<RefreshController>()

    private val NoConnectionString by lazy { getString(R.string.no_connection) }
    private val FailToRefreshString by lazy { getString(R.string.failed_to_refresh) }
    private val SuspendedAccountString by lazy { getString(R.string.http_forbidden) }

    private var isActivityVisible = false
    private var shouldStartMainActivityOnBack = false
    private var refreshLevel = 1
    private var currentlyShownDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loading_activity)
        initLayout()
        initUi()
        refreshController.init()
        refreshController.attachCallback(0, object : Callback {
            override fun onOldApi() {
                currentlyShownDialog = AlertDialog.Builder(this@LoadingActivity).setTitle(R.string.old_api_title).setMessage(R.string.old_api_text).setNeutralButton(R.string.accept, { _, _ ->
                    finish()
                }).show()
            }

            override fun onSuccess(api: Api) {
                if (Build.VERSION.SDK_INT < 17 || !isDestroyed) {
                    refreshLevel++
                    refreshLevel()
                }
            }

            override fun onFail(api: Api) {
                showErrorLayout(FailToRefreshString)
            }

            override fun onLoginFail() {
                showErrorLayout(FailToRefreshString)
            }

            override fun onSuspend() {
                showErrorLayout(SuspendedAccountString)
            }

            override fun onUnauthorized() {
                onFailingOfLogin()
            }

            override fun onLogin() {
            }

        })
        loadData()
    }

    override fun onDestroy() {
        super.onDestroy()
        refreshController.detachCallback(0)
    }

    private fun initLayout() {
        // add the padding from bottom to the layout
        fixLayoutSystemUi(contentLayout)
        // set background
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val bmOptions = Options()
        val scaleFactor = Math.min(1080 / size.x, 1920 / size.y)
        bmOptions.inSampleSize = scaleFactor
        bmOptions.inPreferredConfig = RGB_565
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.loading_bg, bmOptions)
        mainLayout.backgroundDrawable = BitmapDrawable(resources, bitmap)
    }

    private fun initUi() {
        if (databaseController.hasEnoughData()) {
            skipButton.show()
            skipButton.setMargins(left = dip(16))
            logoutButton.setMargins(right = dip(16))
            if (App.DEBUG_NO_LOADING_SCREEN)
                goToMainActivity()
        } else skipButton.hide()
        skipButton.onClick {
            goToMainActivity()
        }
        logoutButton.onClick {
            databaseController.clearData()
            startActivity<LoginActivity>()
            finish()
        }
        retryButton.onClick {
            loadData()
        }

    }

    private fun loadData() {
        hideErrorLayout()
        if (!isNetworkAvailable()) showErrorLayout(NoConnectionString)
        else {
            val pictureFile = File(databaseController.profilePicturePath)
            pictureFile.parentFile?.mkdirs()
            if (!pictureFile.exists())
                apiController.getPicture(databaseController.sessionId, databaseController.userId, pictureFile) successUi {
                    if (databaseController.timetable == null) {
                        apiController.getTimetable(databaseController.sessionId, databaseController.userId) successUi {
                            if (databaseController.groups == null)
                                apiController.getGroups(databaseController.sessionId, databaseController.userId) successUi {
                                    refreshLevel(2)
                                } failUi { processError(it) }
                            else refreshLevel(2)
                        } failUi { processError(it) }
                    } else if (databaseController.groups == null)
                        apiController.getGroups(databaseController.sessionId, databaseController.userId) successUi {
                            refreshLevel(2)
                        } failUi { processError(it) }
                    else refreshLevel(2)
                } failUi { processError(it) }
            else if (databaseController.timetable == null) {
                apiController.getTimetable(databaseController.sessionId, databaseController.userId) successUi {
                    if (databaseController.groups == null)
                        apiController.getGroups(databaseController.sessionId, databaseController.userId) successUi {
                            refreshLevel(2)
                        } failUi { processError(it) }
                    refreshLevel(2)
                } failUi { processError(it) }
            } else if (databaseController.groups == null)
                apiController.getGroups(databaseController.sessionId, databaseController.userId) successUi {
                    refreshLevel(2)
                } failUi { processError(it) }
            else refreshLevel(2)
        }
    }

    private fun processError(exception: Exception) {
        Exception(exception).printStackTrace()
        if (exception.isNeedToLogin())
            loginAgain()
        else if (exception.isForbidden()) {
            showErrorLayout(SuspendedAccountString)
        } else showErrorLayout(FailToRefreshString)
    }

    private fun refreshLevel(level: Int? = null) {
        if (level != null) refreshLevel = level
        if (refreshLevel == -1) loginAgain()
        if (refreshLevel == 1) loadData()
        if (refreshLevel == 2) refreshLevel2()
        else if (refreshLevel == 3) refreshLevel3()
        else if (refreshLevel == 4) refreshLevel4()
        else if (refreshLevel == 5) {
            if (isActivityVisible && (Build.VERSION.SDK_INT < 17 || !isDestroyed))
                goToMainActivity()
            else shouldStartMainActivityOnBack = true
        }
    }

    private fun refreshLevel2() {
        refreshController.refresh(Api.Grades)
    }

    private fun refreshLevel3() {
        refreshController.refresh(Api.BehaveEvents)
    }


    private fun refreshLevel4() {
        refreshController.refresh(Api.Messages)
    }

    private fun goToMainActivity() {
        startActivity<MainActivity>()
        finish()
    }


    private fun loginAgain() {
        refreshLevel = -1
        apiController.login(databaseController.school!!, databaseController.username, databaseController.password, databaseController.year) successNotNullUi {
            databaseController.bulk {
                this.sessionId = it.data.sessionId
                this.userId = it.data.userId
                this.year = it.data.year
            }
            refreshLevel(1)
        } failUi {
            if (it.isNeedToLogin()) {
                databaseController.clearData()
                onFailingOfLogin()
            } else if (it.isForbidden()) {
                showErrorLayout(SuspendedAccountString)
            } else showErrorLayout(FailToRefreshString)
        }
    }

    private fun onFailingOfLogin() {
        databaseController.clearData()
        currentlyShownDialog = AlertDialog.Builder(this)
                .setMessage(R.string.login_data_has_changed)
                .setPositiveButton(R.string.accept) { _, _ ->
                    startActivity<LoginActivity>()
                    finish()
                }.show()
    }

    private fun hideErrorLayout() {
        errorLayout.hide()
        loader.show()
    }

    private fun showErrorLayout(text: String) {
        errorLayout.show()
        loader.hide()
        errorText.text = text
    }

    override fun onResume() {
        super.onResume()
        isActivityVisible = true
        if (shouldStartMainActivityOnBack)
            goToMainActivity()
        currentlyShownDialog?.show()
    }

    override fun onPause() {
        super.onPause()
        isActivityVisible = false
        currentlyShownDialog?.hide()
    }
}