package com.yoavst.mashov.android.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.yoavst.mashov.R
import com.yoavst.mashov.formatDate
import com.yoavst.mashov.model.Grade
import kotlinx.android.synthetic.main.grade_item.view.*
import org.jetbrains.anko.backgroundResource

internal class TestsAdapter(val context: Context, val grades: List<Grade>) : RecyclerView.Adapter<TestsAdapter.TestViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestViewHolder = TestViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.grade_item, parent, false))
    override fun onBindViewHolder(holder: TestViewHolder, position: Int) {
        val grade = grades[position]
        holder.name.text = grade.event
        holder.description.text = grade.subject
        holder.grade.text = grade.grade.toString()
        if (grade.grade >= 95)
            holder.grade.backgroundResource = R.drawable.grade_good
        else if (grade.grade > 55)
            holder.grade.backgroundResource = R.drawable.grade_normal
        else
            holder.grade.backgroundResource = R.drawable.grade_bad
        holder.date.text = grade.eventDate.formatDate()
    }

    override fun getItemCount(): Int = grades.size

    inner class TestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.name
        val description: TextView = itemView.description
        val grade: TextView = itemView.grade
        val date: TextView = itemView.date
    }
}
