package com.yoavst.mashov.android.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yoavst.mashov.android.adapter.TimetableDayAdapter
import com.yoavst.mashov.controller.DatabaseController
import org.jetbrains.anko.support.v4.withArguments
import uy.kohesive.injekt.injectLazy

class TimetableDayFragment : Fragment() {
    private val day by lazy { arguments.getInt(Extra_Day) }
    private val databaseController by injectLazy<DatabaseController>()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return RecyclerView(activity)
    }

    override fun onViewCreated(recyclerView: View, savedInstanceState: Bundle?) {
        recyclerView as RecyclerView
        val hours = databaseController.timetable!!.filter { it.day == day }.sortedBy { it.hour }
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = TimetableDayAdapter(hours) {

        }
    }

    companion object {
        private val Extra_Day = "day"

        fun create(day: Int): TimetableDayFragment {
            return TimetableDayFragment().withArguments(Extra_Day to day)
        }
    }
}