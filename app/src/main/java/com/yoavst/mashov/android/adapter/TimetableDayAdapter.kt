package com.yoavst.mashov.android.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.yoavst.mashov.R
import com.yoavst.mashov.model.Lesson
import kotlinx.android.synthetic.main.timetable_item.view.*
import org.jetbrains.anko.layoutInflater
internal class TimetableDayAdapter(val lessons: List<Lesson>, private val callback: (Lesson) -> Unit) : RecyclerView.Adapter<TimetableDayAdapter.HourViewHolder>() {
    override fun onBindViewHolder(holder: HourViewHolder, position: Int) {
        val item = lessons[position]
        holder.name.text = item.subject
        holder.teacher.text = item.teacher
        holder.number.text = item.hour.toString()
    }

    override fun getItemCount(): Int = lessons.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = HourViewHolder(parent.context.layoutInflater.inflate(R.layout.timetable_item, parent, false))

    class HourViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val number: TextView = itemView.number
        val teacher: TextView = itemView.teacher
        val name: TextView = itemView.name
    }
}