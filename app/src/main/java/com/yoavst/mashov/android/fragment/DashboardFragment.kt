package com.yoavst.mashov.android.fragment

import android.support.v7.widget.LinearLayoutManager
import com.yoavst.mashov.R
import com.yoavst.mashov.android.GradeFormat
import com.yoavst.mashov.android.adapter.TestsAdapter
import com.yoavst.mashov.controller.ApiController.Api
import com.yoavst.mashov.dayOfWeek
import com.yoavst.mashov.model.Lesson
import kotlinx.android.synthetic.main.dashboard_fragment.*
import org.jetbrains.anko.onClick

class DashboardFragment : BaseFragment() {
    override val layoutId: Int = R.layout.dashboard_fragment

    override fun init() {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        hourCountLayout.onClick { act.openFragment(R.id.timetable, backStack = true) }
        averageLayout.onClick { act.openFragment(R.id.averages, backStack = true) }
        messageCountLayout.onClick { act.openFragment(R.id.inbox, backStack = true) }
        moreGrades.onClick { act.openFragment(R.id.grades, backStack = true) }
        onFinishRefresh()

    }

    override fun onFinishRefresh() {
        averageGrade.text = databaseController.grades?.let { GradeFormat.format(it.sumByDouble { it.grade.toDouble() } / it.count { it.grade != 0 }) } ?: "0"
        unreadMessages.text = (databaseController.messages?.let { it.indexOfLast { it.isNew } + 1 } ?: 0).toString()
        val lessonsToday: List<Lesson>? = databaseController.timetable?.filter { it.day == dayOfWeek }
        if (lessonsToday == null)
            hoursToday.text = ""
        else {
            val max = lessonsToday.maxBy { it.hour }?.hour ?: 0
            if (max <= lessonsToday.size) hoursToday.text = max.toString()
            else hoursToday.text = lessonsToday.size.toString()

        }
        val grades = databaseController.grades!!
        recyclerView.adapter = TestsAdapter(activity, grades.take(7))
    }

    override fun refresh() {
        act.refresh(Api.Grades, Api.Messages, Api.BehaveEvents)
    }

    override fun getApi(): Api? = null
}