package com.yoavst.mashov.android.fragment

import com.yoavst.mashov.R
import com.yoavst.mashov.android.adapter.TimetablePagerAdapter
import com.yoavst.mashov.controller.ApiController
import com.yoavst.mashov.dayOfWeek
import kotlinx.android.synthetic.main.timetable_pager_layout.*

class TimetablePagerFragment: BaseFragment() {
    override val layoutId: Int = R.layout.timetable_pager_layout

    override fun refresh() {
        act.refresh(ApiController.Api.Timetable)
    }

    override fun init() {
        toolbar.title = getString(R.string.timetable)
        viewPager.offscreenPageLimit = 6
        onFinishRefresh()
    }

    override fun onFinishRefresh() {
        viewPager.adapter = TimetablePagerAdapter(activity, childFragmentManager)
        tabLayout.setupWithViewPager(viewPager)
        viewPager.currentItem = if (dayOfWeek == 7) 5 else 6 - dayOfWeek
    }

    override fun getApi() = ApiController.Api.Timetable
}