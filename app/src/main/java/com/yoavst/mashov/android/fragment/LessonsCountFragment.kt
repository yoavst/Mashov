package com.yoavst.mashov.android.fragment

import android.support.v7.widget.LinearLayoutManager
import com.yoavst.mashov.R
import com.yoavst.mashov.android.*
import com.yoavst.mashov.android.adapter.LessonsCountAdapter
import com.yoavst.mashov.controller.ApiController
import com.yoavst.mashov.model.BehaveEvent
import com.yoavst.mashov.model.Group
import com.yoavst.mashov.model.LessonCount
import com.yoavst.mashov.model.LessonInfo
import kotlinx.android.synthetic.main.error_layout.*
import kotlinx.android.synthetic.main.lessons_count_layout.*
import org.jetbrains.anko.textResource

class LessonsCountFragment : BaseFragment() {
    override val layoutId: Int = R.layout.lessons_count_layout
    private var hasData = false

    override fun refresh() {
        if (act.refresh(ApiController.Api.LessonsCount, ApiController.Api.Groups, ApiController.Api.BehaveEvents) && (!hasData || databaseController.lessonsCount!!.isEmpty())) showLoading()
    }

    override fun onFinishRefresh() {
        databaseController.lessonsCount?.let { lessonsCount ->
            val groups = databaseController.groups!!
            val events = databaseController.behaveEvents!!
            showContent()
            appBarLayout.show()
            weeklyHours.text = lessonsCount.sumBy { it.weeklyHours }.toString()
            totalHours.text = lessonsCount.sumBy { it.lessonsCount }.toString()
            recyclerView.adapter = LessonsCountAdapter(activity, getLessons(lessonsCount, groups, events))
            recyclerView.post {
                if ((recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() != lessonsCount.size - 1)
                    scrollingLayout.enableScrolling()
                else
                    scrollingLayout.disableScrolling()
            }
        }
    }

    private fun getLessons(lessonsCount: List<LessonCount>, groups: List<Group>, events: List<BehaveEvent>): List<LessonInfo> {
        val lessons = lessonsCount.map { l ->
            val group = groups.firstOrNull { it.groupId == l.groupId }
            if (group == null) null
            else LessonInfo(group.subject, l, 0)
        }.filterNotNull()

        val ids = lessonsCount.map { it.groupId }.toIntArray()
        events.forEach {
            val index = ids.indexOf(it.groupId)
            if (index != -1)
                lessons[index].eventCount++
        }
        return lessons.sortedBy { it.name }
    }


    override fun onFailedToRefresh() {
        if (!hasData) showMessage(R.drawable.ic_report, R.string.no_data)
    }

    override fun init() {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        recyclerView.setDivider()
        toolbar.title = getString(R.string.lessons_count)
        if (databaseController.lessonsCount != null) onFinishRefresh()
        else {
            appBarLayout.hide()
            if (isNetworkAvailable()) {
                refresh()
                showLoading()
            } else
                showMessage(R.drawable.ic_signal_wifi_off, R.string.no_connection_text)
        }
    }

    override fun getApi() = null

    private fun showMessage(drawable: Int, message: Int) {
        errorLayout.show()
        errorProgress.hide()
        recyclerView.hide()
        errorText.show()
        errorImage.show()
        errorText.textResource = message
        errorImage.setImageResource(drawable)
    }

    private fun showContent() {
        errorLayout.hide()
        recyclerView.show()
    }

    private fun showLoading() {
        errorLayout.show()
        errorProgress.show()
        recyclerView.hide()
        errorText.hide()
        errorImage.hide()
    }
}