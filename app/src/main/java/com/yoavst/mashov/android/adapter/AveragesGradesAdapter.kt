package com.yoavst.mashov.android.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder
import com.yoavst.mashov.R
import com.yoavst.mashov.android.GradeFormat
import com.yoavst.mashov.model.Grade
import org.jetbrains.anko.find

internal class AveragesGradesAdapter(val data: List<Pair<String, List<Grade>>>) :
        AbstractExpandableItemAdapter<AveragesGradesAdapter.AverageGradeViewHolder, AveragesGradesAdapter.AverageGradeViewHolder>() {
    val childViewType = 2
    val parentViewType = 3

    init {
        setHasStableIds(true)
    }

    override fun onCheckCanExpandOrCollapseGroup(holder: AverageGradeViewHolder, groupPosition: Int, x: Int, y: Int, expand: Boolean): Boolean {
        return true
    }

    override fun getGroupId(groupPosition: Int): Long = data[groupPosition].first.hashCode().toLong()

    override fun getGroupCount(): Int = data.size

    override fun getGroupItemViewType(groupPosition: Int): Int = parentViewType

    override fun onBindChildViewHolder(holder: AverageGradeViewHolder, groupPosition: Int, childPosition: Int, viewType: Int) {
        val grade = data[groupPosition].second[childPosition]
        holder.name.text = grade.event
        holder.grade.text = grade.grade.toString()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long = (getGroupId(groupPosition) + childPosition)

    override fun onBindGroupViewHolder(holder: AverageGradeViewHolder, groupPosition: Int, viewType: Int) {
        val item = data[groupPosition]
        holder.name.text = item.first
        holder.grade.text = GradeFormat.format(item.second.map { it.grade }.average())
    }

    override fun getChildCount(groupPosition: Int): Int = data[groupPosition].second.size

    override fun onCreateGroupViewHolder(parent: ViewGroup, position: Int): AverageGradeViewHolder =
            AverageGradeViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.average_item, parent, false))

    override fun getChildItemViewType(groupPosition: Int, childPosition: Int): Int = childViewType

    override fun onCreateChildViewHolder(parent: ViewGroup, position: Int): AverageGradeViewHolder =
            AverageGradeViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.average_sub_item, parent, false))


    class AverageGradeViewHolder(view: View) : AbstractExpandableItemViewHolder(view) {
        val name: TextView = itemView.find(R.id.name)
        val grade: TextView = itemView.find(R.id.grade)
    }

}