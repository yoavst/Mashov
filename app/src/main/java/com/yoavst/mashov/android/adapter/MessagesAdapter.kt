package com.yoavst.mashov.android.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.yoavst.mashov.R
import com.yoavst.mashov.android.bold
import com.yoavst.mashov.android.regular
import com.yoavst.mashov.formatLongDate
import com.yoavst.mashov.model.MessageTitle
import kotlinx.android.synthetic.main.messages_item.view.*
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.onClick

internal class MessagesAdapter(val messages: List<MessageTitle>, private val callback: (MessageTitle) -> Unit) : RecyclerView.Adapter<MessagesAdapter.MessageViewHolder>() {
    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val item = messages[position]
        holder.message.text = item.subject
        holder.sender.text = item.senderName
        holder.date.text = item.sendDate.formatLongDate()
        holder.itemView.onClick { callback(item) }
        holder.attachment.visibility = if (item.hasAttachment) View.VISIBLE else View.GONE
        if (item.isNew)
            holder.message.bold()
        else holder.message.regular()
    }

    override fun getItemCount(): Int = messages.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MessageViewHolder(parent.context.layoutInflater.inflate(R.layout.messages_item, parent, false))

    class MessageViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val message: TextView = itemView.message
        val sender: TextView = itemView.sender
        val date: TextView = itemView.date
        val attachment: ImageView = itemView.attachment

    }
}