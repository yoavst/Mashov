package com.yoavst.mashov.android.activity

import android.graphics.Bitmap.Config.RGB_565
import android.graphics.BitmapFactory
import android.graphics.BitmapFactory.Options
import android.graphics.Point
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.ArrayAdapter
import com.yoavst.mashov.R
import com.yoavst.mashov.android.*
import com.yoavst.mashov.android.adapter.SchoolsAdapter
import com.yoavst.mashov.controller.ApiController
import com.yoavst.mashov.controller.DatabaseController
import com.yoavst.mashov.isForbidden
import com.yoavst.mashov.isNeedToLogin
import com.yoavst.mashov.model.LoginData
import com.yoavst.mashov.model.School
import kotlinx.android.synthetic.main.login_activity.*
import nl.komponents.kovenant.ui.failUi
import org.jetbrains.anko.*
import uy.kohesive.injekt.injectLazy

internal class LoginActivity : AppCompatActivity() {
    private val apiController by injectLazy<ApiController>()
    private val databaseController by injectLazy<DatabaseController>()
    private var currentState = -1
    private var isCurrentlyDownloading = false
    private var currentlyShownDialog: AlertDialog? = null
    // School part
    private var schoolAdapter: SchoolsAdapter? = null
    private var originalSchools: List<School>? = null
    // Login part
    private var school: School? = null
    private var year: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)
        initLayout()
        initUI()
        downloadSchools()
    }


    private fun initLayout() {
        // add the padding from bottom to the layout
        fixLayoutSystemUi(layout)
        // set background
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val bmOptions = Options()
        val scaleFactor = Math.min(1080 / size.x, 1920 / size.y)
        bmOptions.inSampleSize = scaleFactor
        bmOptions.inPreferredConfig = RGB_565
        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.welcome_bg, bmOptions)
        mainLayout.backgroundDrawable = BitmapDrawable(resources, bitmap)
    }

    private fun initUI() {
        retryButton.onClick {
            if (currentState == StateSchool) downloadSchools()
            else {
            }
        }
        schoolSearch.textChangedListener {
            onTextChanged { text, _, before, count ->
                val t = text.toString()
                if (text == null || text.isEmpty())
                    schoolAdapter?.schools = originalSchools!!
                else {
                    if (count - before > 0)
                        schoolAdapter!!.schools = schoolAdapter!!.schools.filter { t in it.name.orEmpty() }
                    else if (count != before)
                        schoolAdapter!!.schools = originalSchools!!.filter { t in it.name }
                }
                schoolAdapter!!.notifyDataSetChanged()
            }
        }
        recyclerView.layoutManager = LinearLayoutManager(this)
        idInput.onFocusChange { _, _ -> hideLoginError() }
        password.onFocusChange { _, _ -> hideLoginError() }
        loginButton.onClick {
            hideLoginError()
            if (idInput.text.length < 4) {
                idLayout.isErrorEnabled = true
                idLayout.error = getString(R.string.invalid_id)
            } else if (password.text.length < 5) {
                passwordLayout.isErrorEnabled = true
                passwordLayout.error = getString(R.string.short_password)
            } else login(school!!, year, idInput.text.toString(), password.text.toString())
        }
    }

    //region School
    private fun initSchoolsUi(schools: List<School>) {
        currentState = StateSchool
        hideLoginLayout()
        showSchoolLayout()
        originalSchools = schools
        originalSchools = originalSchools!!.sortedWith(compareBy { it.name })
        schoolAdapter = SchoolsAdapter(originalSchools!!) { school ->
            if (school.years.size == 1) initLoginUi(school, school.years[0])
            else {
                currentlyShownDialog = AlertDialog.Builder(this)
                        .setTitle(R.string.choose_year)
                        .setAdapter(ArrayAdapter<String>(this, android.support.v7.appcompat.R.layout.select_dialog_item_material,
                                school.years.reversed().map { it.toString() })) { _, selection ->
                            currentlyShownDialog = null
                            initLoginUi(school, school.years[school.years.size - 1 - selection])
                        }
                        .setCancelable(true)
                        .setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.cancel(); currentlyShownDialog = null }
                        .show()
            }
        }
        recyclerView.adapter = schoolAdapter
    }

    private fun downloadSchools() {
        showLoadingLayout()
        if (isNetworkAvailable()) {
            if (!isCurrentlyDownloading) {
                isCurrentlyDownloading = true
                apiController.getSchools() successNotNullUi {
                    initSchoolsUi(it)
                    isCurrentlyDownloading = false
                } failUi {
                    it.printStackTrace()
                    isCurrentlyDownloading = false
                    showRetry()
                }
            }
        } else showRetry()
    }
    //endregion

    //region Login
    private fun initLoginUi(school: School, year: Int) {
        currentState = StateLogin
        this.school = school
        this.year = year
        clearFocus()
        hideSchoolLayout()
        showLoginLayout()
    }

    private fun login(school: School, year: Int, id: String, password: String) {
        clearFocus()
        showLoadingLayout()
        if (isNetworkAvailable()) {
            if (!isCurrentlyDownloading) {
                isCurrentlyDownloading = true
                apiController.login(school, id, password, year) successNotNullUi { (data, students) ->
                    val student = students[0]
                    handleUser(id, school, data, student.id, password, student.classCode, student.classNum, student.privateName + " " + student.familyName)
                    isCurrentlyDownloading = false
                } failUi {
                    it.printStackTrace()
                    isCurrentlyDownloading = false
                    showLoginLayout()
                    if (it.isNeedToLogin())
                        toast(R.string.wrong_data)
                    else if (it.isForbidden()) {
                        currentlyShownDialog = AlertDialog.Builder(this)
                                .setMessage(R.string.http_forbidden_text)
                                .show()
                    } else toast(R.string.failed_to_login)
                }
            }
        } else {
            showLoginLayout()
            toast(R.string.no_connection)
        }
    }

    private fun handleUser(username: String, school: School, userDetails: LoginData, userId: String, password: String, classCode: String, classNum: Int, name: String) {
        databaseController.bulk {
            this.id = userDetails.id
            this.password = password
            this.username = username
            this.school = school
            this.sessionId = userDetails.sessionId
            this.userId = userId
            this.year = userDetails.year
            this.classCode = classCode
            this.classNum = classNum
            this.name = name
        }
        startActivity<LoadingActivity>()
        finish()
    }
    //endregion

    //region Lifecycle events
    override fun onPause() {
        super.onPause()
        currentlyShownDialog?.hide()
    }

    override fun onResume() {
        super.onResume()
        currentlyShownDialog?.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        schoolAdapter = null
        originalSchools = null
        currentlyShownDialog = null
    }

    override fun onBackPressed() {
        if (currentState == StateLogin && !isCurrentlyDownloading) {
            hideLoginError()
            initSchoolsUi(originalSchools!!)
        } else super.onBackPressed()
    }

    //endregion

    //region Util
    private fun showRetry() {
        retryButton.show()
        loadingMessage.textResource = R.string.no_connection
    }

    private fun hideLoadingLayout() {
        loadingLayout.hide()
    }

    private fun showLoadingLayout() {
        loadingLayout.show()
        hideSchoolLayout()
        hideLoginLayout()
    }

    private fun hideSchoolLayout() {
        schoolLayout.hide()
    }

    private fun showSchoolLayout() {
        hideLoadingLayout()
        schoolLayout.show()
    }

    private fun hideLoginLayout() {
        loginLayout.hide()
    }

    private fun showLoginLayout() {
        hideLoadingLayout()
        loginLayout.show()
    }

    private fun hideLoginError() {
        idLayout.isErrorEnabled = false
        idLayout.error = null
        passwordLayout.isErrorEnabled = false
        passwordLayout.error = null

    }
    //endregion

    companion object {
        private const val StateSchool = 1
        private const val StateLogin = 2
    }

}