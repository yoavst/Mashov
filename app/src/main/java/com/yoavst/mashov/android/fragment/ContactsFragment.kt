package com.yoavst.mashov.android.fragment

import android.content.Intent
import android.net.Uri
import android.support.design.widget.AppBarLayout
import android.support.v7.widget.LinearLayoutManager
import com.yoavst.mashov.R
import com.yoavst.mashov.android.*
import com.yoavst.mashov.android.adapter.ContactsAdapter
import com.yoavst.mashov.controller.ApiController.Api
import kotlinx.android.synthetic.main.contacts_layout.*
import kotlinx.android.synthetic.main.error_layout.errorImage
import kotlinx.android.synthetic.main.error_layout.errorLayout
import kotlinx.android.synthetic.main.error_layout.errorProgress
import kotlinx.android.synthetic.main.error_layout.errorText
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.email
import org.jetbrains.anko.support.v4.toast

class ContactsFragment : BaseFragment() {
    override val layoutId: Int = R.layout.contacts_layout
    private val classString by lazy { getString(R.string.clazz) }
    private var hasData = false

    override fun refresh() {
        if (act.refresh(Api.Alfon) && !hasData) showLoading()
    }

    override fun onFailedToRefresh() {
        if (!hasData) showMessage(R.drawable.ic_report, R.string.no_data)
    }

    override fun init() {
        toolbar.title = getString(R.string.contacts)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        recyclerView.setDivider()
        if (databaseController.contacts != null) onFinishRefresh()
        else {
            appBarLayout.hide()
            if (isNetworkAvailable()) {
                refresh()
                showLoading()
            } else
                showMessage(R.drawable.ic_signal_wifi_off, R.string.no_connection_text)
        }
    }

    override fun onFinishRefresh() {
            databaseController.contacts?.let { contacts ->
                hasData = true
                appBarLayout.show()
                showContent()
                classname.text = classString + " " + databaseController.classCode + databaseController.classNum.toString()
                school.text = databaseController.school!!.name
                recyclerView.adapter = ContactsAdapter(contacts) { contact, isDial ->
                    if (isDial) {
                        if (contact.cellphone.isNotBlank()) {
                            dial(contact.cellphone)
                        } else if (contact.phone.isNotBlank()) {
                            dial(contact.phone)
                        } else toast(R.string.empty_phone_number)
                    } else {
                        email(contact.email)
                    }
                }
                recyclerView.post {
                    if ((recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() != contacts.size - 1)
                        scrollingLayout.enableScrolling()
                    else
                        scrollingLayout.disableScrolling()

                }
                students.text = contacts.size.toString()
            }
    }

    private fun dial(number: String) {
        startActivity(Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:" + number)))
    }

    override fun getApi() = Api.Alfon

    private fun showMessage(drawable: Int, message: Int) {
        errorLayout.show()
        errorProgress.hide()
        recyclerView.hide()
        errorText.show()
        errorImage.show()
        errorText.textResource = message
        errorImage.setImageResource(drawable)
    }

    private fun showContent() {
        errorLayout.hide()
        recyclerView.show()
    }

    private fun showLoading() {
        errorLayout.show()
        errorProgress.show()
        recyclerView.hide()
        errorText.hide()
        errorImage.hide()
    }
}