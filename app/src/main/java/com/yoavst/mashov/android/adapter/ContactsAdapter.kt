package com.yoavst.mashov.android.adapter

import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.yoavst.mashov.R
import com.yoavst.mashov.android.adapter.ContactsAdapter.ContactsViewHolder
import com.yoavst.mashov.model.Contact
import kotlinx.android.synthetic.main.contacts_item.view.*
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.onClick

internal class ContactsAdapter(val contacts: List<Contact>, private val callback: (Contact, isDial: Boolean) -> Unit) : Adapter<ContactsViewHolder>() {
    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) {
        val item = contacts[position]
        holder.name.text = item.privateName + " " + item.familyName
        holder.address.text = item.address
        holder.dial.onClick { callback(item, true) }
        holder.mail.onClick { callback(item, false) }
    }

    override fun getItemCount(): Int = contacts.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ContactsViewHolder(parent.context.layoutInflater.inflate(R.layout.contacts_item, parent, false))

    class ContactsViewHolder(view: View) : ViewHolder(view) {
        val name: TextView = itemView.name
        val address: TextView = itemView.address
        val dial: ImageView = itemView.dial
        val mail: ImageView = itemView.mail

    }
}