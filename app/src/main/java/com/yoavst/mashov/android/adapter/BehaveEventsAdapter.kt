package com.yoavst.mashov.android.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.style.StrikethroughSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.binaryfork.spanny.Spanny
import com.yoavst.mashov.R
import com.yoavst.mashov.formatDate
import com.yoavst.mashov.model.BehaveEvent
import com.yoavst.mashov.model.Types
import kotlinx.android.synthetic.main.behave_event_item.view.*

internal class BehaveEventsAdapter(context: Context, val events: List<BehaveEvent>) : RecyclerView.Adapter<BehaveEventsAdapter.EventViewHolder>() {
    val lessonString = context.getString(R.string.lesson) + " "
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder = EventViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.behave_event_item, parent, false))
    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        val event = events[position]
        holder.apply {
            hour.text = lessonString + event.lesson
            date.text = event.date.formatDate()
            lesson.text = event.subject + " (" + event.reporter + ")"
            if (event.justificationId == Types.Event_NoJustification)
                name.text = event.text
            else name.text = Spanny(event.text, StrikethroughSpan()).append(" (").append(event.justification).append(")")
        }
    }

    override fun getItemCount(): Int = events.size

    inner class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.name
        val lesson: TextView = itemView.lesson
        val hour: TextView = itemView.hour
        val date: TextView = itemView.date
    }
}
