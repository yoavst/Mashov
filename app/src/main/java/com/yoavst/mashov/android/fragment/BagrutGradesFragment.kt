package com.yoavst.mashov.android.fragment

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.yoavst.mashov.R
import com.yoavst.mashov.android.*
import com.yoavst.mashov.android.adapter.BagrutAdapter
import com.yoavst.mashov.controller.ApiController.Api
import kotlinx.android.synthetic.main.bagrut_layout.*
import kotlinx.android.synthetic.main.error_layout.*
import org.jetbrains.anko.textResource

class BagrutGradesFragment : BaseFragment() {
    override val layoutId: Int = R.layout.bagrut_layout
    private var hasData = false

    override fun refresh() {
        if (act.refresh(Api.BagrutGrades) && (!hasData || databaseController.bagrutGrades!!.isEmpty())) showLoading()
    }

    override fun onFailedToRefresh() {
        if (!hasData) showMessage(R.drawable.ic_report, R.string.no_data)
    }

    override fun init() {
        recyclerView.layoutManager = LinearLayoutManager(activity) as RecyclerView.LayoutManager?
        recyclerView.setHasFixedSize(true)
        recyclerView.setDivider()
        toolbar.title = getString(R.string.bagrut_grades)
        if (databaseController.bagrutGrades != null) onFinishRefresh()
        else {
            appBarLayout.hide()
            if (isNetworkAvailable()) {
                refresh()
                showLoading()
            } else
                showMessage(R.drawable.ic_signal_wifi_off, R.string.no_connection_text)
        }
    }

    override fun onFinishRefresh() {
        if (databaseController.bagrutGrades != null) {
            databaseController.bagrutGrades!!.let { grades ->
                hasData = true
                if (grades.isNotEmpty()) {
                    appBarLayout.show()
                    showContent()
                    grades.filter { it.final != 0 }.let { goodGrades ->
                        averageGrade.text = GradeFormat.format(goodGrades.sumByDouble { it.final.toDouble() } / goodGrades.size)
                        bagrutCount.text = (goodGrades.size).toString()
                    }
                    recyclerView.adapter = BagrutAdapter(activity, grades)
                    recyclerView.post {
                        if ((recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() != grades.size - 1)
                            scrollingLayout.enableScrolling()
                        else
                            scrollingLayout.disableScrolling()
                    }
                } else {
                    appBarLayout.hide()
                    showMessage(R.drawable.ic_report, R.string.no_bagrut)
                }
                Unit
            }
        }
    }

    override fun getApi() = Api.BagrutGrades

    private fun showMessage(drawable: Int, message: Int) {
        errorLayout.show()
        errorProgress.hide()
        recyclerView.hide()
        errorText.show()
        errorImage.show()
        errorText.textResource = message
        errorImage.setImageResource(drawable)
    }

    private fun showContent() {
        errorLayout.hide()
        recyclerView.show()
    }

    private fun showLoading() {
        errorLayout.show()
        errorProgress.show()
        recyclerView.hide()
        errorText.hide()
        errorImage.hide()
    }
}