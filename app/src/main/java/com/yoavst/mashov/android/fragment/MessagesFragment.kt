package com.yoavst.mashov.android.fragment

import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.linroid.filtermenu.library.FilterMenu
import com.yoavst.mashov.R
import com.yoavst.mashov.android.activity.MessageActivity
import com.yoavst.mashov.android.adapter.MessagesAdapter
import com.yoavst.mashov.android.isNetworkAvailable
import com.yoavst.mashov.android.setDivider
import com.yoavst.mashov.controller.ApiController
import com.yoavst.mashov.isForbidden
import com.yoavst.mashov.isNeedToLogin
import com.yoavst.mashov.model.MessageTitle
import kotlinx.android.synthetic.main.messages_layout.*
import nl.komponents.kovenant.ui.failUi
import nl.komponents.kovenant.ui.successUi
import uy.kohesive.injekt.injectLazy
import kotlin.properties.Delegates

class MessagesFragment : BaseFragment() {
    private val apiController by injectLazy<ApiController>()
    override val layoutId: Int = R.layout.messages_layout
    private lateinit var filterMenu: FilterMenu
    private lateinit var originalMessages: List<MessageTitle>
    private lateinit var messages: List<MessageTitle>
    private lateinit var senders: Array<String>
    private var filter: MailFilter = MailFilter.ByDate
    private val messageString by lazy { getString(R.string.message) }
    private var currentlyShowDialog: AlertDialog? = null
    override fun getApi() = ApiController.Api.Messages

    override fun refresh() {
        act.refresh(ApiController.Api.Messages)
    }

    override fun init() {
        toolbar.title = getString(R.string.messages)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        recyclerView.setDivider()
        filterMenu = FilterMenu
                .Builder(activity)
                .addItem(R.drawable.ic_alarm)
                .addItem(R.drawable.ic_attachment_white)
                .addItem(R.drawable.ic_person)
                .withListener(object : FilterMenu.OnMenuChangeListener {
                    override fun onMenuCollapse() {
                    }

                    override fun onMenuItemClick(view: View?, position: Int) {
                        when (position) {
                            0 -> {
                                filter = MailFilter.ByDate
                                applyFilter()
                                filterMenu.collapse(true)
                            }
                            1 -> {
                                filter = MailFilter.AttachmentOnly
                                applyFilter()
                                filterMenu.collapse(true)

                            }
                            2 -> {
                                filterMenu.collapse(false)
                                AlertDialog.Builder(activity)
                                        .setTitle(R.string.choose_sender)
                                        .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                                        .setItems(senders) { dialog, which ->
                                            MailFilter.sender = senders[which]
                                            filter = MailFilter.BySender
                                            applyFilter()
                                            dialog.cancel()
                                        }
                                        .show()
                            }
                        }
                    }

                    override fun onMenuExpand() {
                    }

                })
                .attach(filterMenuLayout)
                .build()

        onFinishRefresh()
    }

    override fun onFinishRefresh() {
        originalMessages = databaseController.messages!!
        senders = originalMessages.map { it.senderName }.distinct().toTypedArray()
        applyFilter()
    }

    fun applyFilter() {
        messages = filter.sort(originalMessages)
        refreshUi()
    }

    private fun refreshUi() {
        recyclerView.adapter = MessagesAdapter(messages) { (messageId) ->
            if (databaseController.hasMessage(messageId)) {
                startMessage(messageId)
            } else if (!isNetworkAvailable()) {
                act.showMessage(act.noConnectionString)
            } else {
                currentlyShowDialog = AlertDialog.Builder(activity)
                        .setMessage(R.string.loading_data)
                        .setCancelable(false)
                        .show()
                apiController.getMessage(databaseController.sessionId, messageId) successUi {
                    currentlyShowDialog?.dismiss()
                    currentlyShowDialog = null
                    startMessage(messageId)
                } failUi {
                    currentlyShowDialog?.dismiss()
                    currentlyShowDialog = null
                    it.printStackTrace()
                    if (it.isNeedToLogin()) {
                        act.showMessage(getString(R.string.please_refresh_before))
                    } else if (it.isForbidden()) {
                        act.onSuspend()
                    } else {
                        act.showMessage(act.errorString.format(messageString))

                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        currentlyShowDialog?.show()
    }

    override fun onPause() {
        super.onPause()
        currentlyShowDialog?.hide()
    }

    private fun startMessage(id: String) {
        MessageActivity.start(activity, id)
    }

    enum class MailFilter {
        ByDate {
            override fun sort(emails: List<MessageTitle>): List<MessageTitle> {
                return emails
            }
        },
        AttachmentOnly {
            override fun sort(emails: List<MessageTitle>): List<MessageTitle> {
                return emails.filter { it.hasAttachment }
            }
        },
        BySender {
            override fun sort(emails: List<MessageTitle>): List<MessageTitle> {
                return emails.filter { it.senderName == sender }
            }
        };

        abstract fun sort(emails: List<MessageTitle>): List<MessageTitle>

        companion object {
            var sender: String by Delegates.notNull()
        }
    }

}