package com.yoavst.mashov.android.activity

import android.app.Activity
import android.os.Bundle
import com.yoavst.mashov.controller.DatabaseController
import org.jetbrains.anko.*
import uy.kohesive.injekt.Injekt
import uy.kohesive.injekt.api.get

internal class OpeningActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val databaseController: DatabaseController = Injekt.get()
        overridePendingTransition(0, 0)
        if (databaseController.id == "" || databaseController.password == "") startActivity<LoginActivity>()
        else startActivity<LoadingActivity>()
        finish()
    }
}