package com.yoavst.mashov.android.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.yoavst.mashov.R
import com.yoavst.mashov.android.adapter.BagrutAdapter.BagrutViewHolder
import com.yoavst.mashov.model.BagrutGrade
import kotlinx.android.synthetic.main.bagrut_item.view.*
import org.jetbrains.anko.layoutInflater


internal class BagrutAdapter(val context: Context, val grades: List<BagrutGrade>) : Adapter<BagrutViewHolder>() {
    val yearly: String = context.getString(R.string.yearly)
    val test: String =  context.getString(R.string.test)
    val finalGrade: String = context.getString(R.string.final_grade)

    override fun onBindViewHolder(holder: BagrutViewHolder, position: Int) {
        val item = grades[position]
        holder.title.text = item.name
        holder.type.text = item.id
        holder.grades.text = "$yearly: ${item.yearly} $test: ${item.test} $finalGrade: ${item.final}"

    }

    override fun getItemCount(): Int = grades.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = BagrutViewHolder(parent.context.layoutInflater.inflate(R.layout.bagrut_item, parent, false))

    class BagrutViewHolder(view: View) : ViewHolder(view) {
        val title: TextView = itemView.title
        val grades: TextView = itemView.grades
        val type: TextView = itemView.type

    }
}