package com.yoavst.mashov.android.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.yoavst.mashov.R
import com.yoavst.mashov.model.LessonInfo
import kotlinx.android.synthetic.main.lessons_count_item.view.*
import org.jetbrains.anko.layoutInflater
internal class LessonsCountAdapter(val context: Context, val lessonsInfo: List<LessonInfo>) : RecyclerView.Adapter<LessonsCountAdapter.LessonsCountViewHolder>() {
    val totalLessons: String = context.getString(R.string.total_lessons) + " "
    val weeklyLessons: String = context.getString(R.string.weekly_lessons) + " "

    override fun onBindViewHolder(holder: LessonsCountViewHolder, position: Int) {
        val item = lessonsInfo[position]
        holder.apply {
            lesson.text = item.name
            eventCount.text = item.eventCount.toString()
            total.text = totalLessons + item.count.lessonsCount.toString()
            weekly.text = weeklyLessons + item.count.weeklyHours.toString()
        }

    }

    override fun getItemCount(): Int = lessonsInfo.size
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = LessonsCountViewHolder(parent.context.layoutInflater.inflate(R.layout.lessons_count_item, parent, false))

    class LessonsCountViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val lesson: TextView = itemView.lesson
        val total: TextView = itemView.total
        val weekly: TextView = itemView.weekly
        val eventCount: TextView = itemView.lessonEventCount
    }
}