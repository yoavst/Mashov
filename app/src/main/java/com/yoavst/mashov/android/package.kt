package com.yoavst.mashov.android

import android.app.Activity
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.os.Build
import android.os.Build.VERSION
import android.support.design.widget.AppBarLayout
import android.support.v4.app.Fragment
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup.MarginLayoutParams
import android.widget.TextView
import com.readystatesoftware.systembartint.SystemBarTintManager
import com.yoavst.mashov.android.view.DividerItemDecoration
import nl.komponents.kovenant.Promise
import nl.komponents.kovenant.ui.successUi
import org.jetbrains.anko.connectivityManager
import org.jetbrains.anko.inputMethodManager
import java.text.DecimalFormat

fun Context.isNetworkAvailable() = connectivityManager.activeNetworkInfo?.isConnected ?: false
fun Fragment.isNetworkAvailable() = activity.connectivityManager.activeNetworkInfo?.isConnected ?: false

fun View.setMargins(left: Int = 0, top: Int = 0, right: Int = 0, bottom: Int = 0) {
    val params = layoutParams as MarginLayoutParams
    params.setMargins(left, top, right, bottom)
    layoutParams = params
}

infix inline fun <K : Any> Promise<K?, Exception>.successNotNullUi(crossinline callback: (K) -> Unit) = successUi { if (it != null) callback(it) }

fun Activity.clearFocus() {
    currentFocus?.let {
        inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0)

    }
}

fun Activity.fixLayoutSystemUi(layout: View) {
    if (VERSION.SDK_INT >= 19) {
        val config = SystemBarTintManager(this).config
        if (VERSION.SDK_INT >= 21) {
            if (!config.isNavigationAtBottom) {
                layout.setMargins(top = config.statusBarHeight)
            } else {
                layout.setMargins(top = config.statusBarHeight, bottom = config.navigationBarHeight)
            }
        }
    }
}

fun Context.drawableRes(id: Int) = ResourcesCompat.getDrawable(resources, id, theme)
fun Context.colorRes(id: Int): Int {
    return if (Build.VERSION.SDK_INT >= 23)
        resources.getColor(id, theme)
    else @Suppress("DEPRECATION") resources.getColor(id)
}


fun View.hide() {
    visibility = View.GONE
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.toggleVisibility(): Unit = if (visibility == View.VISIBLE) hide() else show()

inline infix fun <K> TypedArray.use(init: (typedArray: TypedArray) -> K): K {
    val value = init(this)
    recycle()
    return value
}

fun RecyclerView.setDivider() {
    addItemDecoration(DividerItemDecoration(context, null))
}

val GradeFormat = DecimalFormat("##.#")

fun View.disableScrolling() {
    layoutParams = (layoutParams as AppBarLayout.LayoutParams).apply {
        scrollFlags = 0
    }
}

fun View.enableScrolling() {
    layoutParams = (layoutParams as AppBarLayout.LayoutParams).apply {
        scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS or AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
    }
}

fun TextView.bold() {
    typeface = Typeface.create(typeface, Typeface.BOLD)
}

fun TextView.regular() {
    typeface = Typeface.create(typeface, Typeface.NORMAL)
}