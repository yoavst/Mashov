package com.yoavst.mashov.android.fragment

import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.linroid.filtermenu.library.FilterMenu
import com.yoavst.mashov.R
import com.yoavst.mashov.android.adapter.BehaveEventsAdapter
import com.yoavst.mashov.android.disableScrolling
import com.yoavst.mashov.android.enableScrolling
import com.yoavst.mashov.android.setDivider
import com.yoavst.mashov.controller.ApiController
import com.yoavst.mashov.model.BehaveEvent
import com.yoavst.mashov.model.Types
import kotlinx.android.synthetic.main.behave_events_layout.*
import kotlin.properties.Delegates

class BehaveEventsFragment : BaseFragment() {
    override val layoutId: Int = R.layout.behave_events_layout
    private var filter: BehaveEventsFilter = BehaveEventsFilter.All
    private lateinit var filterMenu: FilterMenu
    private lateinit var originalEvents: List<BehaveEvent>
    private lateinit var events: List<BehaveEvent>
    private lateinit var categories: Array<String>
    override fun refresh() {
        act.refresh(ApiController.Api.BehaveEvents)
    }

    override fun getApi(): ApiController.Api? = ApiController.Api.BehaveEvents


    override fun init() {
        toolbar.title = getString(R.string.behave_events)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        recyclerView.setDivider()
        filterMenu = FilterMenu
                .Builder(activity)
                .addItem(R.drawable.ic_select_all)
                .addItem(R.drawable.ic_done)
                .addItem(R.drawable.ic_clear)
                .addItem(R.drawable.ic_school)
                .withListener(object : FilterMenu.OnMenuChangeListener {
                    override fun onMenuCollapse() {
                    }

                    override fun onMenuItemClick(view: View?, position: Int) {
                        when (position) {
                            0 -> {
                                filter = BehaveEventsFilter.All
                                applyFilter()
                                filterMenu.collapse(true)
                            }
                            1 -> {
                                filter = BehaveEventsFilter.Justified
                                applyFilter()
                                filterMenu.collapse(true)

                            }
                            2 -> {
                                filter = BehaveEventsFilter.Unjustified
                                applyFilter()
                                filterMenu.collapse(true)
                            }
                            3 -> {
                                filterMenu.collapse(false)
                                AlertDialog.Builder(activity)
                                        .setTitle(R.string.choose_lesson)
                                        .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                                        .setItems(categories) { dialog, which ->
                                            BehaveEventsFilter.lesson = categories[which]
                                            filter = BehaveEventsFilter.ByLesson
                                            applyFilter()
                                            dialog.cancel()
                                        }
                                        .show()
                            }
                        }
                    }

                    override fun onMenuExpand() {
                    }

                })
                .attach(filterMenuLayout)
                .build()

        onFinishRefresh()
    }

    override fun onFinishRefresh() {
        originalEvents = databaseController.behaveEvents!!
        categories = originalEvents.map { it.subject }.distinct().toTypedArray().sortedArray()
        applyFilter()
    }

    private fun applyFilter() {
        events = filter.sort(originalEvents)
        refreshUi()

    }

    private fun refreshUi() {
        val tempEvents = if (filter == BehaveEventsFilter.ByLesson) events else originalEvents
        val justifiedCount = tempEvents.count { it.justificationId != Types.Event_NoJustification }
        justifiedEvents.text = justifiedCount.toString()
        unjustifiedEvents.text = (tempEvents.size - justifiedCount).toString()
        recyclerView.adapter = BehaveEventsAdapter(activity, events)
        if ((recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() != events.size - 1)
            scrollingLayout.enableScrolling()
        else
            scrollingLayout.disableScrolling()
    }


    private enum class BehaveEventsFilter {
        All {
            override fun sort(events: List<BehaveEvent>): List<BehaveEvent> {
                return events
            }
        },
        Justified {
            override fun sort(events: List<BehaveEvent>): List<BehaveEvent> {
                return events.filter { it.justificationId != Types.Event_NoJustification }
            }
        },
        Unjustified {
            override fun sort(events: List<BehaveEvent>): List<BehaveEvent> {
                return events.filter { it.justificationId == Types.Event_NoJustification }
            }
        },
        ByLesson {
            override fun sort(events: List<BehaveEvent>): List<BehaveEvent> {
                return events.filter { it.subject == lesson }
            }
        };

        abstract fun sort(events: List<BehaveEvent>): List<BehaveEvent>

        companion object {
            var lesson: String by Delegates.notNull()
        }
    }
}