package com.yoavst.mashov.android.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yoavst.mashov.R
import com.yoavst.mashov.android.activity.MainActivity
import com.yoavst.mashov.android.drawableRes
import com.yoavst.mashov.controller.DatabaseController
import com.yoavst.mashov.model.Refreshable
import org.jetbrains.anko.support.v4.findOptional
import uy.kohesive.injekt.injectLazy

abstract class BaseFragment : Fragment(), Refreshable {
    protected val databaseController by injectLazy<DatabaseController>()
    protected abstract val layoutId: Int
    protected val toolbarId: Int = R.id.toolbar
    protected val act: MainActivity
        get() {
            return activity as MainActivity
        }

    open fun init() {
    }

    protected abstract fun refresh()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initToolbar(findOptional<Toolbar>(toolbarId))
        init()
    }

    private fun initToolbar(toolbar: Toolbar?) {
        if (toolbar != null) {
            toolbar.navigationIcon = activity.drawableRes(R.drawable.ic_menu)
            toolbar.setNavigationOnClickListener { act.openDrawer() }
            activity.menuInflater.inflate(R.menu.main, toolbar.menu)
            toolbar.menu.findItem(R.id.refresh).setOnMenuItemClickListener { refresh(); true }
            toolbar.menu.findItem(R.id.logout).setOnMenuItemClickListener { act.logout() ;true }
        }
    }
}