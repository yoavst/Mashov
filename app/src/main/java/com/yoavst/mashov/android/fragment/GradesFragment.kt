package com.yoavst.mashov.android.fragment

import android.animation.LayoutTransition
import android.graphics.Color
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import com.linroid.filtermenu.library.FilterMenu
import com.yoavst.mashov.R
import com.yoavst.mashov.android.*
import com.yoavst.mashov.android.adapter.TestsAdapter
import com.yoavst.mashov.controller.ApiController
import com.yoavst.mashov.model.Grade
import kotlinx.android.synthetic.main.grades_layout.*
import lecho.lib.hellocharts.model.*
import org.jetbrains.anko.support.v4.toast
import java.util.*
import kotlin.properties.Delegates

class GradesFragment : BaseFragment() {
    override val layoutId: Int = R.layout.grades_layout
    private lateinit var filterMenu: FilterMenu
    private var hasChartInit = false
    private lateinit var originalGrades: List<Grade>
    private lateinit var grades: List<Grade>
    private lateinit var categories: Array<String>
    private var filter: GradeFilter = GradeFilter.ByDate
    private var averageValue: Double = 0.0

    override fun getApi(): ApiController.Api = ApiController.Api.Grades

    override fun init() {
        toolbar.inflateMenu(R.menu.tests)
        toolbar.title = getString(R.string.grades)
        val switchItem = toolbar.menu.findItem(R.id.action_switch)
        switchItem.setOnMenuItemClickListener { switchChart() }
        appBarLayout.layoutTransition.addTransitionListener(object : LayoutTransition.TransitionListener {
            override fun startTransition(transition: LayoutTransition, container: ViewGroup, view: View, transitionType: Int) {
            }

            override fun endTransition(transition: LayoutTransition, container: ViewGroup, view: View, transitionType: Int) {
                if (transitionType == LayoutTransition.CHANGE_DISAPPEARING) {
                    recyclerView.show()
                }
            }
        })
        chartView.isZoomEnabled = false
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)
        recyclerView.setDivider()
        filterMenu = FilterMenu
                .Builder(activity)
                .addItem(R.drawable.ic_alarm)
                .addItem(R.drawable.ic_achievement)
                .addItem(R.drawable.ic_school)
                .withListener(object : FilterMenu.OnMenuChangeListener {
                    override fun onMenuCollapse() {
                    }

                    override fun onMenuItemClick(view: View?, position: Int) {
                        when (position) {
                            0 -> {
                                filter = GradeFilter.ByDate
                                applyFilter()
                                filterMenu.collapse(true)
                            }
                            1 -> {
                                filter = GradeFilter.ByGrade
                                applyFilter()
                                filterMenu.collapse(true)

                            }
                            2 -> {
                                filterMenu.collapse(false)
                                AlertDialog.Builder(activity)
                                        .setTitle(R.string.choose_lesson)
                                        .setNegativeButton(R.string.cancel) { dialog, _ -> dialog.cancel() }
                                        .setItems(categories) { dialog, which ->
                                            GradeFilter.lesson = categories[which]
                                            filter = GradeFilter.ByLesson
                                            applyFilter()
                                            dialog.cancel()
                                        }
                                        .show()
                            }
                        }
                    }

                    override fun onMenuExpand() {
                    }

                })
                .attach(filterMenuLayout)
                .build()

        onFinishRefresh()
    }

    override fun refresh() {
        act.refresh(ApiController.Api.Grades)
    }

    override fun onFinishRefresh() {
        originalGrades = databaseController.grades!!
        categories = originalGrades.map { it.subject }.distinct().toTypedArray().sortedArray()
        applyFilter()
    }

    private fun refreshUi() {
        averageValue = grades.sumByDouble { it.grade.toDouble() } / grades.count { it.grade != 0 }
        recyclerView.adapter = TestsAdapter(context, grades)
        setupData(grades.filter { it.grade != 0 }.size, averageValue)
        if (grades.size > 1) {
            if (chartView.visibility == View.VISIBLE)
                drawChart()
            else {
                if ((recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() != grades.size - 1)
                    scrollingLayout.enableScrolling()
                else
                    scrollingLayout.disableScrolling()
                hasChartInit = false
            }
        } else if (chartView.visibility == View.VISIBLE) {
            toggleVisibility()
            fixScrolling()
        }
    }


    private fun setupData(tests: Int, average: Double) {
        testsCount.text = tests.toString()
        averageGrade.text = GradeFormat.format(average)
    }

    private fun switchChart(): Boolean {
        if (chartView.visibility == View.GONE) {
            if (grades.size > 1) {
                recyclerView.hide()
                if (!hasChartInit) {
                    drawChart()
                    hasChartInit = true
                }
            } else {
                toast(R.string.less_than_two_grades)
                return true
            }
        }
        toggleVisibility()
        fixScrolling()
        return true
    }

    private fun toggleVisibility() {
        if (chartView.visibility == View.VISIBLE) {
            chartView.hide()
            appBarLayout.layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
            appBarLayout.requestLayout()
        } else {
            chartView.show()
            appBarLayout.layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT
            appBarLayout.requestLayout()
        }
    }

    private fun fixScrolling() {
        if (chartView.visibility == View.VISIBLE)
            scrollingLayout.disableScrolling()
        else if ((recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() != grades.size - 1)
            scrollingLayout.enableScrolling()
        else
            scrollingLayout.disableScrolling()
    }

    private fun drawChart() {
        val values = ArrayList<PointValue>(grades.size)
        val averageValues = ArrayList<PointValue>(grades.size)
        var pos = 0
        for (grade in grades.reversed()) {
            if (grade.grade != 0) {
                val text = grade.subject + " " + grade.event
                values.add(PointValue(pos.toFloat(), grade.grade.toFloat()).setLabel(text.substring(0, Math.min(32, text.length))))
                averageValues.add(PointValue(pos.toFloat(), averageValue.toFloat()))
                pos++
            }
        }
        val lines = ArrayList<Line>(2)
        lines.add(Line(values).setColor(activity.colorRes(R.color.accent)).setHasLines(true).setHasLabelsOnlyForSelected(true))
        lines.add(Line(averageValues).setColor(Color.WHITE).setHasLines(true).setHasPoints(false))
        val data = LineChartData()
        data.lines = lines
        val axisY = Axis.generateAxisFromRange(0F, 100F, 5F).setAutoGenerated(false).setHasLines(true).setTextColor(Color.WHITE)
        data.axisYLeft = axisY
        chartView.lineChartData = data
        chartView.isValueSelectionEnabled = true
        val v = Viewport(chartView.maximumViewport)
        v.bottom = 0F
        v.top = 100F
        chartView.maximumViewport = v
        chartView.currentViewport = v
    }

    fun applyFilter() {
        grades = filter.sort(originalGrades)
        refreshUi()
    }

    private enum class GradeFilter {
        ByDate {
            override fun sort(grades: List<Grade>): List<Grade> {
                return grades
            }
        },
        ByGrade {
            override fun sort(grades: List<Grade>): List<Grade> {
                val comparator = Comparator<Grade> { firstGrade, secondGrade -> (secondGrade.grade - firstGrade.grade) }
                return grades.sortedWith(comparator)
            }
        },
        ByLesson {
            override fun sort(grades: List<Grade>): List<Grade> {
                return grades.filter { it.subject == lesson }
            }
        };

        abstract fun sort(grades: List<Grade>): List<Grade>

        companion object {
            var lesson: String by Delegates.notNull()
        }
    }
}