package com.yoavst.mashov.android.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.yoavst.mashov.R
import com.yoavst.mashov.android.fragment.TimetableDayFragment

internal class TimetablePagerAdapter(context: Context, fragmentManager: FragmentManager): FragmentPagerAdapter(fragmentManager) {
    val Titles = context.resources.getStringArray(R.array.week_days)
    override fun getItem(position: Int): Fragment {
        return TimetableDayFragment.create(6 - position)
    }

    override fun getCount(): Int = 6

    override fun getPageTitle(position: Int) = Titles[5 - position]
}