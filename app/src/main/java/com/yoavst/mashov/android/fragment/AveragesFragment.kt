package com.yoavst.mashov.android.fragment

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager
import com.h6ah4i.android.widget.advrecyclerview.utils.BaseWrapperAdapter
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils
import com.linroid.filtermenu.library.FilterMenu
import com.yoavst.mashov.R
import com.yoavst.mashov.android.GradeFormat
import com.yoavst.mashov.android.adapter.AveragesGradesAdapter
import com.yoavst.mashov.android.disableScrolling
import com.yoavst.mashov.android.enableScrolling
import com.yoavst.mashov.controller.ApiController
import com.yoavst.mashov.model.Grade
import kotlinx.android.synthetic.main.grades_layout.*
import java.util.*

class AveragesFragment : BaseFragment() {
    override val layoutId: Int = R.layout.grades_layout
    private lateinit var filterMenu: FilterMenu
    private lateinit var originalData: List<Pair<String, List<Grade>>>
    private lateinit var data: List<Pair<String, List<Grade>>>
    private var isByABC = true
    private lateinit var itemManager: RecyclerViewExpandableItemManager


    override fun init() {
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(false)
        filterMenu = FilterMenu
                .Builder(activity)
                .addItem(R.drawable.ic_sort_alphabetical)
                .addItem(R.drawable.ic_achievement)
                .withListener(object : FilterMenu.OnMenuChangeListener {
                    override fun onMenuCollapse() {
                    }

                    override fun onMenuItemClick(view: View?, position: Int) {
                        when (position) {
                            0 -> {
                                isByABC = true
                                applyFilter()
                                refreshUi()
                                filterMenu.collapse(true)
                            }
                            1 -> {
                                isByABC = false
                                applyFilter()
                                refreshUi()
                                filterMenu.collapse(true)

                            }
                        }
                    }

                    override fun onMenuExpand() {
                    }

                })
                .attach(filterMenuLayout)
                .build()
        onFinishRefresh()
    }

    override fun refresh() {
        act.refresh(ApiController.Api.Grades)
    }

    override fun onFinishRefresh() {
        val grades = databaseController.grades!!.filter { it.grade != 0 }
        val categories = grades.map { it.subject }.distinct()
        originalData = categories.map { category ->
            category.to(grades.filter { it.subject == category })
        }
        testsCount.text = grades.size.toString()
        averageGrade.text = GradeFormat.format(grades.sumByDouble { it.grade.toDouble() } / grades.size)
        applyFilter()
        refreshUi()
    }

    fun applyFilter() {
        data = if (isByABC) {
            val comparator = Comparator<Pair<String, List<Grade>>> { first, second -> first.first.compareTo(second.first) }
            originalData.sortedWith(comparator)
        } else {
            originalData.sortedByDescending { it.second.sumByDouble { it.grade.toDouble() } / it.second.size }
        }
    }

    fun refreshUi() {
        if (recyclerView.adapter != null) {
            itemManager.release()
        }
        itemManager = RecyclerViewExpandableItemManager(null)
        itemManager.attachRecyclerView(recyclerView)
        val adapter = AveragesGradesAdapter(data)
        val wrapperAdapter = itemManager.createWrappedAdapter(adapter)
        val animator = RefactoredDefaultItemAnimator()
        animator.supportsChangeAnimations = false
        recyclerView.adapter = wrapperAdapter
        recyclerView.itemAnimator = animator
        recyclerView.post {
            if (view != null) {
                if ((recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition() != data.size - 1)
                    scrollingLayout.enableScrolling()
                else
                    scrollingLayout.disableScrolling()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        itemManager.release()
        val adapter = (recyclerView.adapter as? BaseWrapperAdapter)
        adapter?.let {
            WrapperAdapterUtils.releaseAll(it)
        }
    }

    override fun getApi(): ApiController.Api = ApiController.Api.Grades
}