package com.yoavst.mashov.injection

import com.yoavst.mashov.controller.*
import uy.kohesive.injekt.api.InjektModule
import uy.kohesive.injekt.api.InjektRegistrar
import uy.kohesive.injekt.api.fullType
import uy.kohesive.injekt.api.get

object ControllerInjectionModule : InjektModule {
    override fun InjektRegistrar.registerInjectables() {
        addSingleton(fullType<CookieManager>(), CookieManagerImpl)
        addSingleton(fullType<RequestsController>(), RequestsControllerImpl)
        addSingleton(fullType<DataParser>(), DataParserImpl)
        addSingleton(fullType<ApiController>(), ApiControllerImpl(get(), get(), get()))
        addSingleton(fullType<DatabaseController>(), DatabaseControllerWrapper(DatabaseControllerImpl))
        addSingleton(fullType<RefreshController>(), RefreshControllerImpl)
    }
}