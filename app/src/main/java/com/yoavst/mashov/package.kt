package com.yoavst.mashov

import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.HttpException
import java.text.SimpleDateFormat
import java.util.*

fun Exception.isUnauthorized() = ((this as? FuelError)?.exception as? HttpException)?.httpCode ?: 0 == 401
fun Exception.IsInternalServerError() = ((this as? FuelError)?.exception as? HttpException)?.httpCode ?: 0 == 500
fun Exception.isNeedToLogin() = isUnauthorized() || IsInternalServerError()

fun Exception.isForbidden() = ((this as? FuelError)?.exception as? HttpException)?.httpCode ?: 0 == 403

val dayOfWeek: Int
    get() {
        return Calendar.getInstance()[Calendar.DAY_OF_WEEK]
    }

private val DateFormat = SimpleDateFormat("dd.MM.yy")
private val LongDateFormat = SimpleDateFormat("HH:mm dd.MM.yy")

val StoragePermission = arrayOf("android.permission.WRITE_EXTERNAL_STORAGE")

fun Long.formatDate() = DateFormat.format(Date(this))
fun Long.formatLongDate() = LongDateFormat.format(Date(this))
