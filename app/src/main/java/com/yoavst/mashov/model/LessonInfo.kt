package com.yoavst.mashov.model

internal data class LessonInfo(val name: String, val count: LessonCount, var eventCount: Int)