package com.yoavst.mashov.model

import com.yoavst.mashov.controller.ApiController.Api

internal interface Refreshable {
    fun onFinishRefresh()
    fun onFailedToRefresh() {}
    fun getApi(): Api?
}